import * as url from 'url';
import path from 'path';

import {glob} from 'glob';
import pgp from 'pg-promise';
import patterns from 'pg-promise/lib/patterns.js';

import Connection from './connection.js';
import Relation from './relation.js';
import Executable from './executable.js';
import {$order} from '../statement/index.js';
import Expr, {Tuple, Literal} from '../statement/expr.js';
import Values from '../statement/values.js';
import {$join} from '../statement/join.js';
import identity from '../util/identity.js';

const cwd = url.fileURLToPath(new URL('.', import.meta.url));
const introspectors = {
  relations: new pgp.QueryFile(`${cwd}/../sql/tables.sql`),
  functions: new pgp.QueryFile(`${cwd}/../sql/functions.sql`)
};

export default class Database {
  #scripts_dir;
  #attached = new Map();
  #query_files = new Set();
  #connection;

  $join = $join;
  $order = $order;

  constructor(connection, driver_config = {}) {
    this.pgp = pgp(driver_config);
    this.#scripts_dir = path.resolve(connection.scripts || 'sql');
    this.#connection = new Connection(this.pgp(connection));

    this.$target = this.#connection.$target;
    this.$pool = this.#connection.instance.$pool;

    // forward the query-running methods from Connection
    this.query = this.#connection.query.bind(this.#connection);
    this.task = this.#connection.task.bind(this.#connection);
    this.transaction = this.#connection.transaction.bind(this.#connection);
    this.select = this.#connection.select.bind(this.#connection);
    this.insert = this.#connection.insert.bind(this.#connection);
    this.update = this.#connection.update.bind(this.#connection);
    this.delete = this.#connection.delete.bind(this.#connection);
    this.execute = this.#connection.execute.bind(this.#connection);
    this.save = this.#connection.save.bind(this.#connection);
    this.all = this.#connection.all.bind(this.#connection);
  }

  [Symbol.iterator]() {
    return this.#attached[Symbol.iterator]();
  }

  async reload() {
    for (const key of this.#attached.keys()) {
      let path = key.split('.');
      let name = path.pop();
      let pointer = this;

      for (const segment of path) {
        pointer = this[segment];
      }

      delete pointer[name];

      this.#attached.delete(key);
    }

    for (const rel of (await this.#connection.query(introspectors.relations))) {
      this.attach(new Relation(rel), [rel.schema, rel.name].filter(identity));
    }

    for (const fn of (await this.#connection.query(introspectors.functions))) {
      this.attach(new Executable(fn), [fn.schema, fn.name].filter(identity));
    }

    const matches = await glob(`${this.#scripts_dir}/**/*.sql`);

    matches.forEach(m => {
      const qf = [...this.#query_files].find(qf => qf.file === m) || new pgp.QueryFile(m, {minify: true});

      if (qf.error) {
        throw qf.error;
      }

      this.#query_files.add(qf);
      this.attach(new Executable(qf), [
        ...path.relative(this.#scripts_dir, path.dirname(m)).split(path.sep),
        path.basename(m, '.sql')
      ].filter(i => i));
    });

    return this;
  }

  attach(entity, path) {
    const joined = path.join('.');

    if (this.#attached.get(joined)) {
      // TODO support merging at least of a function or script over a relation,
      // throw otherwise
      console.warn(`${entity.constructor.name} ${entity.name} collided with existing ${this.#attached.get(joined).constructor.name} on path db.${joined}!`);
    }

    let pointer = this;
    let idx = -1;

    while (++idx < path.length - 1) {
      if (!pointer[path[idx]]) {
        pointer[path[idx]] = {}; // nothing here yet, fill in the node
      }

      pointer = pointer[path[idx]];
    }

    pointer[path[idx]] = entity;

    this.#attached.set(joined, entity);
  }

  expr() {
    return new Expr(...arguments);
  }

  tuple() {
    return new Tuple(...arguments);
  }

  literal() {
    return new Literal(...arguments);
  }

  values(alias, ...objs) {
    return new Values(alias, ...objs);
  }

  count(expr) {
    if (expr) {
      return new Expr(`count(${expr.compile()})`).as(expr.alias || 'count');
    }

    return new Expr(`count(*)`).as('count');
  }
};
