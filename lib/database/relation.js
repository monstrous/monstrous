import Statement from '../statement/index.js';

export default class Relation {
  #columns;
  #primary_key;
  #foreign_keys;

  constructor (spec, name) {
    this.schema = spec.schema;
    this.name = name || spec.name;
    this.qualified = [];

    if (spec instanceof Relation) {
      this.#primary_key = spec.#primary_key;
      this.#foreign_keys = spec.#foreign_keys && spec.#foreign_keys.map(fk => ({
        ...fk,
        reference_schema: spec.schema,
        reference: spec.name // keys are always de-aliased!
      }));

      this.#columns = spec.#columns;

      for (const c of Object.keys(this.#columns)) {
        const q = this.qualify(c);

        this.qualified.push(q);
        this[`$${c}`] = q;
      }
    } else {
      this.#primary_key = spec.primary_key;
      this.#foreign_keys = spec.foreign_keys;

      this.#columns = {};

      for (const c of spec.columns) {
        this.#columns[c.name] = c;

        const q = this.qualify(c.name);

        this.qualified.push(q);
        this[`$${c.name}`] = q;
      }
    }
  }

  get [Symbol.toStringTag]() {
    return 'Relation';
  }

  *[Symbol.iterator]() {
    for (const column of this.qualified) {
      yield column;
    }
  }

  get primary_key() {
    return this.qualify(this.#primary_key);
  }

  get path() {
    if (this.schema) {
      return `"${this.schema}"."${this.name}"`;
    }

    return `"${this.name}"`;
  }

  qualify(name) {
    if (Array.isArray(name)) {
      return name.map(n => `${this.path}."${n}"`);
    }

    return `${this.path}."${name}"`;
  }

  equals(schema, name) {
    return this.schema === schema && this.name === name;
  }

  has(column) {
    return !!this.#columns[column];
  }

  split_by_pk(obj) {
    if (this.#primary_key) {
      return Object.entries(obj).reduce((acc, [k, v]) => {
        if (this.#primary_key.indexOf(k) === -1) {
          acc[0][k] = v; // normal column
        } else {
          acc[1][k] = v; // pk column
        }

        return acc;
      }, [{}, {}])
    }

    return [obj, {}];
  }

  type(column) {
    return this.#columns[column].type;
  }

  typcategory(column) {
    return this.#columns[column].category;
  }

  fks_to(relation) {
    return this.#foreign_keys && this.#foreign_keys.filter(fk =>
      relation.equals(fk.origin_schema, fk.origin)
    );
  }

  as(alias) {
    return new Alias(this, alias);
  }

  only() {
    return new Statement(this).only(...arguments);
  }

  join() {
    return new Statement(this).join(...arguments);
  }

  filter() {
    return new Statement(this).filter(...arguments);
  }

  distinct() {
    return new Statement(this).distinct(...arguments);
  }

  project() {
    return new Statement(this).project(...arguments);
  }

  group() {
    return new Statement(this).group(...arguments);
  }

  order() {
    return new Statement(this).order(...arguments);
  }

  offset() {
    return new Statement(this).offset(...arguments);
  }

  limit() {
    return new Statement(this).limit(...arguments);
  }
}

export class Alias extends Relation {
  #relname;

  constructor(relation, alias) {
    super(relation, alias);

    this.#relname = relation.name;
  }

  *[Symbol.iterator]() {
    for (const column of this.qualified) {
      yield column;
    }
  }

  get [Symbol.toStringTag]() {
    return 'Alias';
  }

  equals(schema, relname) {
    return this.schema === schema && this.#relname === relname;
  }

  get path() {
    if (this.schema) {
      return `"${this.schema}"."${this.#relname}"`;
    }

    return `"${this.#relname}"`;
  }

  qualify(name) {
    return `"${this.name}"."${name}"`;
  }
}
