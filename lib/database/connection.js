import pgp from 'pg-promise';

import Relation from './relation.js';
import Statement from '../statement/index.js';

export default class Connection {
  $target = {
    one: (function () {
      return this.instance.query(...arguments, pgp.queryResult.one).then(
        rows => {
          if (Array.isArray(rows)) return rows[0];

          return rows;
        }
      );
    }).bind(this),
    list: (function () {
      return this.instance.query(...arguments).then(rows => {
        if (rows.length > 0) {
          const key = Object.keys(rows[0])[0];

          return rows.map(row => row[key]);
        }

        return rows;
      });
    }).bind(this),
    unit: (function () {
      return this.instance.query(...arguments, pgp.queryResult.one).then(rows => {
        const row = Array.isArray(rows) ? rows[0] : rows;

        return row[Object.keys(row)[0]];
      });
    }).bind(this),
    stream: () => {}, // TODO
    log: sql => sql, // build and return query text
  };

  constructor(instance) {
    this.instance = instance;
    this.query = function () {
      const args = [...arguments];

      const sql = args.shift();
      let target = this.instance.query;

      if (typeof args[args.length - 1] === 'function') {
        target = args.pop();
      }

      if (args.length === 1 && Object.prototype.toString.call(args[0]) === '[object Object]') {
        return target(sql, args[0]);
      }

      return target(sql, args);
    }
  }

  // TODO settings to apply GUCs at beginning of task/tx
  task(fn, tag) {
    const args = pgp.utils.taskArgs([{tag}, task => {
      const conn = new Connection(task);

      return fn(conn);
    }]);

    return this.instance.task.apply(this, args);
  }

  transaction(fn, mode, tag) {
    const args = pgp.utils.taskArgs([{mode, tag}, tx => {
      const conn = new Connection(tx);

      return fn(conn);
    }]);

    return this.instance.tx.apply(this, args);
  }

  select(statement, target = this.instance.query) {
    if (statement instanceof Relation) {
      statement = new Statement(statement);
    }

    const compiled = statement.select();

    const result = target(compiled.sql, compiled.params, compiled.qrm);

    if (target === this.instance.query) {
      return result.then(r => statement.articulate(r));
    } else if (target === this.$target.one) {
      return result.then(r => {
        const articulated = statement.articulate(r);

        if (Array.isArray(articulated)) return articulated[0];

        return articulated;
      });
    }

    return result;
  }

  all(...statements) {
    return this.instance.task.apply(this, [task => {
      const conn = new Connection(task);

      return Promise.all(
        statements.map(s => conn.select(s))
      )
    }]);
  }

  insert(statement, ...values) { // final value may be a query-target function!
    let target = this.instance.query;

    if (typeof values[values.length - 1] === 'function') {
      target = values.pop();
    }

    if (values.length === 0) {
      return Promise.resolve([]);
    }

    if (statement instanceof Relation) {
      statement = new Statement(statement);
    }

    const compiled = statement.insert(...values);

    return target(compiled.sql, compiled.params, compiled.qrm);
  }

  update(statement, changes, target = this.instance.query) {
    if (statement instanceof Relation) {
      statement = new Statement(statement);
    }

    const compiled = statement.update(changes);

    return target(compiled.sql, compiled.params, compiled.qrm);
  }

  save(relation, value, target = this.$target.one) {
    if (!(relation instanceof Relation)) {
      throw new Error('save may only be invoked for relations, not statements');
    }

    const split = relation.split_by_pk(value);

    if (split[1] && Object.keys(split[1]).length > 0) {
      return this.update(relation.filter(split[1]), split[0], target);
    }

    return this.insert(relation, value, target);
  }

  delete(statement, target = this.instance.query) {
    if (statement instanceof Relation) {
      statement = new Statement(statement);
    }

    const compiled = statement.delete();

    return target(compiled.sql, compiled.params, compiled.qrm);
  }

  execute(executable, ...args) {
    let target;

    if (typeof args[args.length - 1] === 'function') {
      target = args.pop();
    } else {
      target = this.instance.query;
    }

    return target(...executable.construct(...args));
  }
};
