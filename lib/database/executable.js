import pgp from 'pg-promise';

import Expr from '../statement/expr.js';

export default class Executable {
  #qrm = pgp.queryResult.any;

  constructor (spec) {
    if (spec instanceof pgp.QueryFile) {
      this.sql = spec;
    } else {
      this.schema = spec.schema;
      this.name = spec.name;
      this.args = spec.args;

      // functions can be overloaded or variadic, so we build the sql text at
      // runtime with however many arguments we've been given
      if (spec.is_procedure) {
        this.#qrm = pgp.queryResult.none;
        this.sql = params => `call ${this.path}(${params})`;
      } else {
        this.sql = params => `select * from ${this.path}(${params})`;

        if (spec.is_single_row) {
          this.#qrm = pgp.queryResult.one;
        }

        // TODO single value/unit
      }
    }
  }

  get path() {
    if (this.schema) {
      return `"${this.schema}"."${this.name}"`;
    }

    return `"${this.name}"`;
  }

  construct() {
    const args = [...arguments];

    if (this.sql instanceof pgp.QueryFile) {
      if (args.length === 1 && Object.prototype.toString.call(args[0]) === '[object Object]') {
        return [this.sql, args[0], this.#qrm];
      }

      // queryfiles can't use exprs so no need to process those here
      return [this.sql, args, this.#qrm];
    }

    let idx = 1

    if (args.length > this.args.length) {
      throw new Error(`too many arguments for function ${this.name}!`);
    }

    const [placeholders, values] = args.reduce(([p, v], arg, arg_idx) => {
      if (arg instanceof Expr) {
        // TODO it's theoretically possible to push arg.sql onto p and concat
        // arg.values onto v, which would avoid the full inlining and be safer
        // against SQL injection, but exprs can use their own ordinals for
        // placeholders which could collide.
        //
        // we strongly recommend against user input in exprs anyway.
        p.push(arg.compile());
      } else {
        const current = this.args[arg_idx];

        if (['json', 'jsonb'].includes(current.type)) {
          if (current.is_array) {
            // mostly for completeness' sake; please use json arrays instead
            const [arrplaceholders, arrvalues] = arg.reduce(([ap, av], val) => {
              ap.push(`$${idx++}`);
              av.push(JSON.stringify(val));

              return [ap, av];
            }, [[], []]);
            p.push(`array[${arrplaceholders.join(', ')}]::${current.type}[]`)
            v = v.concat(arrvalues);
          } else {
            p.push(`$${idx++}::${current.type}`);
            v.push(JSON.stringify(arg));
          }
        } else if (['uuid'].includes(current.type)) {
          // uuids have implicit casts to text, but that avails little when
          // resolving functions by signature
          p.push(`$${idx++}::${current.type}${current.is_array ? '[]' : ''}`);
          v.push(arg);
        } else {
          p.push(`$${idx++}`);
          v.push(arg);
        }
      }

      return [p, v];
    }, [[], []]);

    return [this.sql(placeholders), values, this.#qrm];
  }
}
