select distinct -- ignore overloads (quick version)
  case
    when n.nspname = current_schema then null
    else n.nspname
  end as "schema",
  p.proname as "name",
  p.provariadic as "is_variadic",
  (p.prokind = 'p') as "is_procedure",
  (not p.proretset) as "is_single_row",
  (t.typtype in ('b', 'd', 'e', 'r')) as "is_single_value",
  jsonb_agg(
    jsonb_build_object(
      'type', coalesce(argarrt.typname, argt.typname),
      'is_array', argarrt.oid is not null
    ) order by arg.ordinality
  ) as args
from pg_proc as p
join pg_namespace as n on p.pronamespace = n.oid
join pg_type as t on p.prorettype = t.oid
left outer join lateral unnest(p.proargtypes) with ordinality as arg (oid) on true
left outer join pg_type as argt on argt.oid = arg.oid
left outer join pg_type as argarrt on argarrt.typarray = argt.oid
where n.nspname not in ('pg_catalog', 'information_schema')
group by n.nspname, p.proname, p.provariadic, p.prokind, p.proretset, t.typtype;
