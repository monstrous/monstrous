export default function find_last(arr, cb) {
  for (let i = arr.length - 1; i >= 0; i--) {
    if (cb(arr[i])) {
      return arr[i];
    }
  }

  return null;
}
