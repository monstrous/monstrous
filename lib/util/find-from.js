export default function find_from(arr, start, fn) {
  for (let i = start; i < arr.length; i++) {
    if (fn(arr[i])) return arr[i];
  }
}
