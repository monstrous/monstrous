import Expr, {Tuple} from '../statement/expr.js';
import find_from from '../util/find-from.js';
import quote from '../util/quote.js';
import unquote from '../util/unquote.js';

export function insert_values(
  relation,
  values,
  conflict,
  attrs = Object.keys(values[0]).reduce((acc, attr) => {
    const name = unquote(attr.split('.').pop());
    if (relation.has(name)) acc.push(name);
    return acc;
  }, [])
) {
  const text = [];
  const params = [];
  const lists = [];
  let param_idx = 1;

  text.push(`insert into ${relation.path}`);
  text.push(`(${attrs.join(', ')})`);
  text.push('values');

  for (const val_obj of values) {
    const placeholders = [];

    for (const attr of attrs) {
      if (val_obj[attr] instanceof Tuple) {
        placeholders.push(`$${param_idx++}`);
        params.push(val_obj[attr].compile(true));
      } else if (val_obj[attr] instanceof Expr) {
        placeholders.push(val_obj[attr].compile());
      } else {
        placeholders.push(`$${param_idx++}`);
        params.push(val_obj[attr]);
      }
    }

    lists.push(`(${placeholders.join(', ')})`);
  }

  text.push(lists.join(', '));

  if (conflict) {
    // TODO on conflict
    // text.push(`on conflict ${conflict.target} do ${conflict.action()}`);
  }

  text.push('returning *');

  return {
    sql: text.join(' '),
    params,
    param_idx
  };
}

// entry point for generating a series of insert-select statements after an
// initial insert-values for the root relation
export function insert_tree(root_relation, joins, values, param_idx) {
  if (!Array.isArray(values)) values = [values];

  const text = [];
  const params = [];
  const rel_names = Object.keys(values[0]).filter(k => !root_relation.has(k));

  for (const name of rel_names) {
    const join = joins.find(j =>
      j.relation.name === name ||
      j.relation.path === name
    );

    if (!join) continue;

    // cannot reference a relation joined earlier from a later join!
    const {
      sql: inner_sql,
      params: inner_params,
      param_idx: new_param_idx
    } = insert_select(joins, join, values[0][name], param_idx, 0);

    text.push(`, ${inner_sql}`);
    Array.prototype.push.apply(params, inner_params);
    param_idx = new_param_idx;
  }

  return {
    sql: text.join(' '),
    params,
    param_idx
  };
};

export function insert_select(joins, join, values, param_idx, min_join_idx) {
  if (!Array.isArray(values)) values = [values];

  const {
    rel_names,
    own_attrs
  } = Object.keys(values[0]).reduce((acc, k) => {
    if (join.relation.has(k)) {
      acc.own_attrs.push(k);
    } else if (joins.some(j => j.relation.name === k)) {
      acc.rel_names.push(k);
    }

    return acc;
  }, {rel_names: [], own_attrs: []});

  if (values.length > 1 && rel_names.length > 0) {
    throw new Error('multi-table inserts may only add multiple records to leaf tables');
  }

  const text = [];
  const params = [];

  const fks = join.on.involved_fields(join.relation);
  const into_fields = Object.keys(fks).map(f => f.split('.').pop());
  const from_fields = Object.values(fks).map(f => {
    const split = f.split('.');
    const field = split.pop();
    const rel = unquote(split.pop());

    return `${unquote(rel)}_cte.${field}`;
  });

  text.push(`${join.relation.name}_cte as (`)
  text.push(`insert into ${join.relation.path}`);
  text.push(`(${into_fields.concat(own_attrs.map(quote)).join(', ')})`);
  text.push('select');
  text.push(from_fields.concat(own_attrs.map(a => `${join.relation.name}_values.${a}`)).join(', '));
  text.push('from (values');

  const [records, args] = values.reduce((acc, leaf) => {
    const placeholders = own_attrs.map(attr => {
      if (leaf[attr] instanceof Expr) {
        return leaf[attr].compile();
      }

      // we're building insert-selects that join user-provided values lists
      // instead of direct insert-values in order to support deeper record
      // graphs, but this means Postgres loses the type info so we have to cast
      // column values for uuids, dates, etc
      switch (join.relation.typcategory(attr)) {
        case 'S': // string
        case 'N': // numeric
        case 'B': // boolean
          return `$${param_idx++}`;
        default:
          return `$${param_idx++}::${join.relation.type(attr)}`;
      }
    }).join(', ');

    return [
      acc[0].concat(`(${placeholders})`),
      acc[1].concat(own_attrs.map(a => leaf[a]))
    ]
  }, [[], []]);

  text.push(records.join(', '));
  Array.prototype.push.apply(params, args);

  text.push(`) as ${join.relation.name}_values (${own_attrs.join(', ')})`);

  for (const rel of Object.values(fks).reduce((acc, f) => acc.add(f.split('.').slice(-2, -1)[0]), new Set())) {
    text.push(`cross join ${unquote(rel)}_cte`)
  }

  text.push('returning *');
  text.push(')');

  for (const name of rel_names) {
    const join = find_from(joins, min_join_idx, j => [j.relation.name, j.relation.path].indexOf(name) > -1);
    const val = values[0][name];

    // cannot reference a relation joined earlier from a later join!
    const {
      sql: inner_sql,
      params: inner_params,
      param_idx: new_param_idx
    } = insert_select(min_join_idx + 1, join, val, param_idx);

    text.push(`, ${inner_sql}`);
    Array.prototype.push.apply(params, inner_params);
    param_idx = new_param_idx;
  }

  return {
    sql: text.join(' '),
    params,
    param_idx
  };
}
