import Relation from '../database/relation.js';
import Statement from './index.js';
import Values from './values.js';
import PredicateTree from './predicate-tree.js';
import find_last from '../util/find-last.js';

export const $join = {
  inner: Symbol('inner join'),
  left: Symbol('left outer join')
  // TODO right, cross, lateral, anti?
};

export default class Join {
  #latest_parent; // latest join our `on` criteria reference
  #on_values = [];

  relation;
  type = $join.inner;
  on;

  constructor(tree, joinable, ...args) {
    if (!(
      joinable instanceof Relation ||
      joinable instanceof Statement ||
      joinable instanceof Values
    )) {
      throw new Error('bad join definition: not a relation, statement, or values list');
    }

    this.relation = joinable;

    if (!tree) return; // origin relation
    else {
      for (const j of tree) {
        if (j.relation === joinable) {
          throw new Error(`bad join definition: ${joinable.name} is repeated`);
        }
      }
    }

    this.on = new PredicateTree(
      tree.reduce(
        (acc, j) => typeof j.relation[Symbol.iterator] === 'function' ? acc.concat([...j.relation]) : acc,
        typeof joinable[Symbol.iterator] === 'function' ? [...joinable] : []
      ).flat()
    );

    let a;
    while (a = args.shift()) {
      this.type = typeof a === 'symbol' && a || this.type;
      if (typeof a !== 'symbol') {
        this.on.add(a);

        // track both keys and values -- columns can appear on either side
        Array.prototype.push.apply(this.#on_values, Object.keys(a))
        Array.prototype.push.apply(this.#on_values, Object.values(a))
      }
    }

    if (this.on.is_active) {
      this.#set_latest_parent(tree);
    } else {
      if (joinable instanceof Statement || joinable instanceof Values) {
        throw new Error('bad join definition: non-tables require explicit `on` criteria');
      }

      this.#set_default_on(tree);
    }
  }

  get #is_relation() {
    return this.relation instanceof Relation;
  }

  #set_latest_parent(tree) {
    this.#latest_parent = find_last(
      tree,
      j => {
        // TODO values?
        if (j.relation instanceof Statement) {
          return [...j.relation].some(q => this.#on_values.indexOf(q) > -1);
        } else if (j.relation.qualified) {
          return j.relation.qualified.some(q => this.#on_values.indexOf(q) > -1);
        }
      }
    );
  }

  #set_default_on(tree) {
    // no explicit condition, look for foreign keys we could use
    // for each previous joined relation:
    const candidates = tree.reduce((acc, j) => {
      if (!j.#is_relation) return acc;

      return acc.concat(
        // include any foreign keys pointing to the new relation
        ...(j.relation.fks_to(this.relation) || []),
        // and any current-relation fks originating here
        ...(this.relation.fks_to(j.relation) || []),
      )
    }, []);

    switch (candidates.length) {
      case 1:
        const fk = candidates[0];

        if (this.relation.equals(fk.origin_schema, fk.origin)) {
          // a foreign key on an earlier relation referencing the new relation
          this.#latest_parent = find_last(
            tree,
            j => j.#is_relation && j.relation.equals(fk.reference_schema, fk.reference)
          );

          this.on.add(fk.reference_columns.reduce((acc, column, idx) => {
            acc[this.#latest_parent.relation[`$${column}`]] =
              this.relation[`$${fk.origin_columns[idx]}`];

            return acc;
          }, {}));
        } else {
          // a foreign key on the new relation referencing an earlier relation
          this.#latest_parent = find_last(
            tree,
            j => j.#is_relation && j.relation.equals(fk.origin_schema, fk.origin)
          );

          this.on.add(fk.reference_columns.reduce((acc, column, idx) => {
            acc[this.relation[`$${column}`]] =
              this.#latest_parent.relation[`$${fk.origin_columns[idx]}`];

            return acc;
          }, {}));
        }

        break;
      case 0: throw new Error(`no join condition and no available foreign key relationship for ${this.relation.name}`);
      default: throw new Error(`multiple possible foreign keys for ${this.relation.name}, specify an explicit join condition`);
    }
  }

  get path() {
    const parents = [this.relation.name];

    // joins on constant conditions don't set #latest_parent
    if (!this.#latest_parent) return parents;

    let pointer = this.#latest_parent;

    // walk backward through join parents to figure out our collection path
    do {
      parents.unshift(pointer.relation.name);

      pointer = pointer.#latest_parent;
    } while (pointer);

    return parents.slice(1); // omit the root relation
  }
}
