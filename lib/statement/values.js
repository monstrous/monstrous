import quote from '../util/quote.js';

export default class Values {
  #objs;

  name;

  constructor (name, ...objs) {
    if (objs.length === 0) {
      throw new Error('cannot use empty values list');
    }

    this.#objs = objs;

    this.name = name;

    for (const key of Object.keys(this.#objs[0])) {
      this[`$${key}`] = `"${this.name}".${key}`;
    }
  }

  *[Symbol.iterator]() {
    for (const key of Object.keys(this.#objs[0])) {
      yield `"${this.name}"."${key}"`;
    }
  }

  get [Symbol.toStringTag]() {
    return 'Values';
  }

  get keys() {
    return Object.keys(this.#objs[0]).map(quote).join(', ');
  }

  get projection() {
    return Object.keys(this.#objs[0]).reduce((acc, key) => {
      acc[key] = `"${this.name}".${quote(key)}`;

      return acc;
    }, {});
  }

  compile(param_idx) {
    const record = Array(Object.keys(this.#objs[0]).length).fill(0);

    return {
      sql: Array(this.#objs.length).fill(0)
        .map(() => `(${record.map(() => `$${param_idx++}`).join(', ')})`)
        .join(', '),
      params: this.#objs.reduce((acc, o) => {
        Array.prototype.push.apply(acc, Object.values(o));

        return acc;
      }, [])
    }
  }
}
