import pgp from 'pg-promise';

export default class Expr {
  alias;

  constructor (sql, ...params) {
    this.sql = sql;

    if (params.length === 1 && Object.prototype.toString.call(params[0]) === '[object Object]') {
      this.params = params[0];
    } else {
      this.params = params;
    }
  }

  get [Symbol.toStringTag]() {
    return 'Expr';
  }

  as(alias) {
    this.alias = alias;

    return this;
  }

  options(format_options) {
    this.format_options = format_options;

    return this;
  }

  compile() {
    return pgp.as.format(this.sql, this.params, this.format_options || {});
  }
}

export class Tuple extends Expr {
  constructor() {
    super();

    this.params = [...arguments];
  }

  get [Symbol.toStringTag]() {
    return 'Tuple';
  }

  compile(raw) {
    // tuples only use ordinal parameters since the SQL is constructed entirely
    // behind monstrous' api surface
    const placeholders = this.params.map((p, idx) => {
      if (p instanceof Expr) return `$${idx + 1}:raw`;

      return `$${idx + 1}`;
    }).join(', ');

    return {
      rawType: raw === true,
      toPostgres: () => pgp.as.format(
        `(${placeholders})`,
        this.params.map(p => {
          if (p instanceof Expr) {
            return p.compile(true); // unused in Expr, but also covers nested Tuples
          }

          return p;
        }),
        this.format_options || {}
      )
    };
  }
}

export class Literal extends Expr {
  // literal value (boolean, number, text) escaped and formatted; always
  // quoted text, but Postgres has built-in conversions for bools, integers,
  // and floating-point numbers
  constructor() {
    super(...arguments);
  }

  get [Symbol.toStringTag]() {
    return 'Literal';
  }

  compile() {
    return pgp.as.text(this.sql);
  }
}
