import pgp from 'pg-promise';
import Relation from '../database/relation.js';
import Expr, {Tuple} from './expr.js';
import quote from '../util/quote.js';
import unquote from '../util/unquote.js';

function prefix(path, name) {
  return (path || []).concat([name]).join('__');
}

function get_key(record, attrs, path) {
  if (!Array.isArray(attrs)) {
    attrs = [attrs];
  }

  return attrs.reduce((acc, a) => {
    const key = prefix(path, a);

    return acc.concat(!record || record[key] == null ? [] : [record[key]]);
  }, []).join('-');
}

function name_columns(columns, initial) {
  return columns.reduce((acc, c) => {
    if (c instanceof Expr) {
      if (!c.alias) {
        throw new Error('projected exprs must be aliased with as()');
      }

      acc[c.alias] = c;
    } else if (c) {
      const name = unquote(c.split('.').pop());

      acc[name] = c;
    }

    return acc;
  }, initial || {});
}

export default class Projection {
  #key;
  #columns;

  #to_array;
  #projections = {};

  constructor(key, columns = {}, to_array = true) {
    if (Array.isArray(key)) {
      this.#key = key.map(k => unquote(k.split('.').pop()));
    } else if (key) {
      this.#key = unquote(key.split('.').pop());
    }

    if (Array.isArray(columns)) {
      this.#columns = name_columns(columns);
    } else {
      this.#columns = columns;
    }

    this.#to_array = to_array;
  }

  get is_complex() {
    return Object.keys(this.#projections).length > 0;
  }

  static from(proj) {
    const created = new Projection();

    if (Object.prototype.toString.call(proj) === '[object Relation]') {
      return Projection.from({
        $key: proj.primary_key,
        $fields: proj.qualified
      });
    }

    for (const [name, val] of Object.entries(proj)) {
      if (name === '$key') {
        created.#key = [];

        for (const qualified of (Array.isArray(val) ? val : [val])) {
          const name = unquote(qualified.split('.').pop());
          created.#key.push(name);
          created.#columns[name] = qualified;
        }

        continue;
      }

      if (name === '$fields') {
        created.#columns = name_columns(val, created.#columns);

        continue;
      }

      switch (Object.prototype.toString.call(val)) {
        case '[object Object]':
        case '[object Relation]':
          // object or relation descendant
          created.#projections[name] = Projection.from(proj[name]);
          created.#projections[name].#to_array = false;
          break;
        default:
          if (Array.isArray(val)) {
            // array descendant, can also be an array containing a relation!
            created.#projections[name] = Projection.from(proj[name][0]);
          } else {
            // aliased column
            created.#columns[name] = val;
          }

          break;
      }
    }

    return created;
  }

  augment(path, relation, to_array=true) {
    const name = path.pop();
    let pointer = this;

    for (const p of path) {
      pointer = pointer.#projections[p];
    }

    if (relation instanceof Relation) {
      pointer.#projections[name] = new Projection(
        relation.primary_key,
        relation.qualified,
        to_array
      );
    } else if (relation.projection) {
      if (relation.projection instanceof Projection) {
        // subquery
        pointer.#projections[name] = relation.projection.clone();
        pointer.#projections[name].#columns =
          Object.entries(pointer.#projections[name].#columns).reduce(
            (acc, [name, qualified]) => {
              const realiasing = qualified.split('.');
              realiasing[0] = quote(relation.name);

              acc[name] = realiasing.join('.');

              return acc;
            },
            {}
          );
      } else {
        // values list
        pointer.#projections[name] = new Projection(
          null,
          Object.entries(relation.projection).reduce((acc, [key, q]) => {
            if (!key.startsWith('#')) {
              const name = q.split('.').pop();

              acc[unquote(name)] = `"${relation.name}".${name}`;
            }

            return acc;
          }, {}),
          false
        );
      }
    }
  }

  columns() {
    return Object.keys(this.#columns);
  }

  flatten(path = []) {
    const refs = [];

    for (const [alias, field] of Object.entries(this.#columns)) {
      if (field instanceof Tuple) {
        field.alias = field.alias || alias;

        refs.push(
          pgp.as.format(
            `$1:raw as "${prefix(path, alias)}"`,
            field.compile(true)
          )
        );
      } else if (field instanceof Expr) {
        // exprs defined as values in projection mapping {alias: db.expr(...)}
        // don't have to be aliased (it'd be redundant) but that means when
        // articulating, their alias is undefined -- so set it explicitly!
        field.alias = field.alias || alias;

        refs.push(`${field.compile()} as "${prefix(path, alias)}"`);
      } else {
        refs.push(`${field} as "${prefix(path, alias)}"`);
      }
    }

    for (const [alias, nested] of Object.entries(this.#projections)) {
      Array.prototype.push.apply(refs, nested.flatten(path.concat(alias)));
    }

    return refs;
  }

  clone() {
    const clone = new Projection(this.#key, this.#columns, this.#to_array);

    for (const [name, p] of Object.entries(this.#projections)) {
      clone.#projections[name] = p.clone();
    }

    // non-#projection members of a nested #projection are columns or exprs
    // which will not change

    return clone;
  }

  articulate(result) {
    if (!result) {
      return [];
    } else if (result.length === 0 || Object.keys(this.#projections).length === 0) {
      return result; // nothing to articulate one way or the other
    }

    /* Generate a nested dictionary of id:entity in the form of the final
     * structure we're trying to build, effectively hashing ids to ensure we
     * don't duplicate any entities in cases where multiple dependent tables are
     * joined into the source query.

     * Output: {1: {id: 1, name: 'hi', children: {111: {id: 111, name: 'ih'}}}
     */
    const mapping = (Array.isArray(result) ? result : [result]).reduce((acc, row) => {
      if (!get_key(row, this.#key)) {
        throw new Error('attempted to articulate a row with a null or undefined root key; ensure that all columns are aliased uniquely and/or a complex projection schema defines $key for each output object or array');
      }

      return (function build (obj, proj, path) {
        const key = get_key(row, proj.#key, path);

        if (proj.#key && !key) {
          // if we declared a #key but can't find a value, it's an entity that
          // doesn't exist (e.g. from an outer join); if we _didn't_ declare a
          // #key, it could be a subquery
          return undefined;
        } else if (!Object.prototype.hasOwnProperty.call(obj, key)) {
          obj[key] = {}; // this entity is new
        }

        obj = Object.entries(proj.#columns).reduce((mapped, [name, val]) => {
          switch (Object.prototype.toString.call(val)) {
            case '[object String]': // column
              obj[key][name] = row[prefix(path, name)];
              break;
            case '[object Expr]':
            case '[object Tuple]':
            case '[object Literal]':
              obj[key][name] = row[prefix(path, val.alias)];
              break;
            default:
              break;
          }

          return mapped;
        }, obj);

        for (const [alias, nested] of Object.entries(proj.#projections)) {
          if (nested.#to_array) {
            obj[key][alias] = build(obj[key][alias] || {}, nested, path.concat([alias]));
          } else {
            const descendant = build(obj[key][alias] || {}, nested, path.concat([alias]));

            if (descendant != undefined) {
              obj[key][alias] = descendant;
            }
          }
        }

        return obj;
      })(acc, this, []);
    }, {});

    /* Build the final graph. The structure and data already exists in mapping,
     * but we need to transform the {id: entity} structures into arrays of
     * entities (or flat objects if required) from the schema.
     *
     * Output: [{id: 1, name: 'hi', children: [{id: 111, name: 'ih'}]}] */
    return (function transform (proj, map, accumulator) {
      // for every id:entity pair in the current level of mapping, if the schema
      // defines any dependent entities recurse and transform them, then push the
      // current object into the output and return

      if (map == undefined) {
        return accumulator;
      }

      return Object.entries(map).reduce((acc, [key, val]) => {
        const transformed = Object.entries(proj.#projections).reduce((obj, [alias, nested]) => {
          if (nested.#to_array) {
            obj[alias] = transform(nested, val[alias], []);
          } else {
            if (val[alias]) obj[alias] = transform(nested, val[alias], {});
          }

          return obj;
        }, val);

        if (Object.prototype.toString.call(acc) === '[object Object]') {
          acc = transformed;
        } else if (Array.isArray(acc)) {
          acc.push(transformed);
        }

        return acc;
      }, accumulator);
    })(this, mapping, []);
  };
}
