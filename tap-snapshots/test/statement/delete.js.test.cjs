/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/delete.js TAP deletes and returns a record > matches query text 1`] = `
delete from "regular_table" where "regular_table"."id" = $1 returning "regular_table".*
`

exports[`test/statement/delete.js TAP deletes and returns a record > regular_table matches results 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/delete.js TAP deletes and returns a record as an object > matches query text 1`] = `
delete from "regular_table" where "regular_table"."id" = $1 returning "regular_table".*
`

exports[`test/statement/delete.js TAP deletes and returns a record as an object > regular_table matches results 1`] = `
Object {
  "bool": false,
  "id": 2,
  "inserted_at": {time},
  "num": 2.1,
  "txt": "two",
}
`

exports[`test/statement/delete.js TAP deletes and returns multiple records > matches query text 1`] = `
delete from "regular_table" where bool is $1 returning "regular_table".*
`

exports[`test/statement/delete.js TAP deletes and returns multiple records > regular_table matches results 1`] = `
Array [
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`
