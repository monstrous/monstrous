/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/order.js TAP column order > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/order.js TAP column order > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by "regular_table"."id" desc
`

exports[`test/statement/order.js TAP expr default order > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/order.js TAP expr default order > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by id
`

exports[`test/statement/order.js TAP expr directed order with args > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/order.js TAP expr directed order with args > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by id + 3 desc
`

exports[`test/statement/order.js TAP expr order > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/order.js TAP expr order > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by id desc
`

exports[`test/statement/order.js TAP expr order with args > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/order.js TAP expr order with args > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by id + 3
`

exports[`test/statement/order.js TAP function(column) order > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/order.js TAP function(column) order > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by "regular_table"."id" desc
`

exports[`test/statement/order.js TAP order direction asc is implicit > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/order.js TAP order direction asc is implicit > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by "regular_table"."id"
`

exports[`test/statement/order.js TAP sort on multiple columns > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
]
`

exports[`test/statement/order.js TAP sort on multiple columns > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by "regular_table"."bool" desc, "regular_table"."num" asc
`

exports[`test/statement/order.js TAP unqualified string order > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/order.js TAP unqualified string order > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by id desc
`
