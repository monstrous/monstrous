/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/filter.js TAP filter column to column > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/filter.js TAP filter column to column > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where "regular_table"."id" = "regular_table"."num"
`

exports[`test/statement/filter.js TAP filter column to column with operation > matches query result 1`] = `
Array []
`

exports[`test/statement/filter.js TAP filter column to column with operation > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where "regular_table"."id" > "regular_table"."num"
`

exports[`test/statement/filter.js TAP filter in subquery > matches query result 1`] = `
Array [
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
]
`

exports[`test/statement/filter.js TAP filter in subquery > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where txt in (select "join_parent"."txt" as "txt" from "join_parent" where "join_parent"."id" > $1)
`

exports[`test/statement/filter.js TAP filter json with subscripting > matches query result 1`] = `
Array [
  Object {
    "id": 1,
    "inserted_at": {time},
    "js": Object {
      "alpha": 1,
      "beta": Object {
        "gamma": 2,
      },
    },
  },
]
`

exports[`test/statement/filter.js TAP filter json with subscripting > matches query text 1`] = `
select "json_table"."id" as "id", "json_table"."js" as "js", "json_table"."inserted_at" as "inserted_at" from "json_table" where "json_table"."js"['alpha']::int = $1
`

exports[`test/statement/filter.js TAP filter json with traversal > matches query result 1`] = `
Array [
  Object {
    "id": 1,
    "inserted_at": {time},
    "js": Object {
      "alpha": 1,
      "beta": Object {
        "gamma": 2,
      },
    },
  },
]
`

exports[`test/statement/filter.js TAP filter json with traversal > matches query text 1`] = `
select "json_table"."id" as "id", "json_table"."js" as "js", "json_table"."inserted_at" as "inserted_at" from "json_table" where ("json_table"."js" ->> 'alpha')::int = $1
`

exports[`test/statement/filter.js TAP filter not-in array criteria > matches query result 1`] = `
Array [
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/filter.js TAP filter not-in array criteria > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num not in ($1, $2, $3)
`

exports[`test/statement/filter.js TAP filter overrides op for nulls > matches query result 1`] = `
Array [
  Object {
    "ettin_id": null,
    "id": 1,
    "txt": "eerht",
  },
]
`

exports[`test/statement/filter.js TAP filter overrides op for nulls > matches query text 1`] = `
select "join_ettin"."id" as "id", "join_ettin"."ettin_id" as "ettin_id", "join_ettin"."txt" as "txt" from "join_ettin" where ettin_id is $1
`

exports[`test/statement/filter.js TAP filter with combination of criteria and expr > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/filter.js TAP filter with combination of criteria and expr > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where txt = $1 and bool is $2 and num <= 2.5
`

exports[`test/statement/filter.js TAP filter with compound criteria > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
]
`

exports[`test/statement/filter.js TAP filter with compound criteria > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where txt = $1 and bool is $2
`

exports[`test/statement/filter.js TAP filter with explicit array criteria > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/filter.js TAP filter with explicit array criteria > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num in ($1, $2, $3)
`

exports[`test/statement/filter.js TAP filter with expr > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/filter.js TAP filter with expr > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num <= 2.5
`

exports[`test/statement/filter.js TAP filter with expr criteria value > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/filter.js TAP filter with expr criteria value > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where "regular_table"."num" <= 5.0 / 2
`

exports[`test/statement/filter.js TAP filter with implicit array criteria > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/filter.js TAP filter with implicit array criteria > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num in ($1, $2, $3)
`

exports[`test/statement/filter.js TAP filter with int primary key > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/filter.js TAP filter with int primary key > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where "regular_table"."id" = $1
`

exports[`test/statement/filter.js TAP filter with interpolated-column expr > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/filter.js TAP filter with interpolated-column expr > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where "regular_table"."num" <= 2.5
`

exports[`test/statement/filter.js TAP filter with named-parameterized expr > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/filter.js TAP filter with named-parameterized expr > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num <= 2.5
`

exports[`test/statement/filter.js TAP filter with nested conjunction > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/filter.js TAP filter with nested conjunction > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where txt = $1 and bool is $2
`

exports[`test/statement/filter.js TAP filter with nested disjunction > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/filter.js TAP filter with nested disjunction > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where txt = $1 or bool is $2
`

exports[`test/statement/filter.js TAP filter with operation criteria > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/filter.js TAP filter with operation criteria > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where bool is $1
`

exports[`test/statement/filter.js TAP filter with parameterized expr > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/filter.js TAP filter with parameterized expr > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num <= 2.5
`

exports[`test/statement/filter.js TAP filter with string > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/filter.js TAP filter with string > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num <= 2.5
`

exports[`test/statement/filter.js TAP logically impossible key combinations can happen > matches query result 1`] = `
Array []
`

exports[`test/statement/filter.js TAP logically impossible key combinations can happen > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where txt = $1 and txt = $2
`
