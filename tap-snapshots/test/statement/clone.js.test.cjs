/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/clone.js TAP filter clones the previous statement > must match snapshot 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table"
`

exports[`test/statement/clone.js TAP filter clones the previous statement > must match snapshot 2`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where bool is $1
`

exports[`test/statement/clone.js TAP filter clones the previous statement > must match snapshot 3`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where bool is $1 and "regular_table"."id" = $2
`

exports[`test/statement/clone.js TAP filter clones the previous statement > must match snapshot 4`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP filter clones the previous statement > must match snapshot 5`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP filter clones the previous statement > must match snapshot 6`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/clone.js TAP group clones the previous statement > must match snapshot 1`] = `
select 1 as "one" from "regular_table"
`

exports[`test/statement/clone.js TAP group clones the previous statement > must match snapshot 2`] = `
select 1 as "one" from "regular_table" group by "regular_table"."bool"
`

exports[`test/statement/clone.js TAP group clones the previous statement > must match snapshot 3`] = `
select 1 as "one" from "regular_table" group by "regular_table"."id"
`

exports[`test/statement/clone.js TAP group clones the previous statement > must match snapshot 4`] = `
Array [
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
]
`

exports[`test/statement/clone.js TAP group clones the previous statement > must match snapshot 5`] = `
Array [
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
]
`

exports[`test/statement/clone.js TAP group clones the previous statement > must match snapshot 6`] = `
Array [
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
  Object {
    "one": 1,
  },
]
`

exports[`test/statement/clone.js TAP join clones the previous statement > must match snapshot 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt" from "join_parent"
`

exports[`test/statement/clone.js TAP join clones the previous statement > must match snapshot 2`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_junction"."parent_id" as "join_junction__parent_id", "join_junction"."ettin_id" as "join_junction__ettin_id" from "join_parent" inner join "join_junction" on "join_junction"."parent_id" = "join_parent"."id"
`

exports[`test/statement/clone.js TAP join clones the previous statement > must match snapshot 3`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_junction"."parent_id" as "join_junction__parent_id", "join_junction"."ettin_id" as "join_junction__ettin_id", "join_ettin"."id" as "join_junction__join_ettin__id", "join_ettin"."ettin_id" as "join_junction__join_ettin__ettin_id", "join_ettin"."txt" as "join_junction__join_ettin__txt" from "join_parent" inner join "join_junction" on "join_junction"."parent_id" = "join_parent"."id" inner join "join_ettin" on "join_junction"."ettin_id" = "join_ettin"."id"
`

exports[`test/statement/clone.js TAP join clones the previous statement > must match snapshot 4`] = `
Array [
  Object {
    "id": 1,
    "txt": "one",
  },
  Object {
    "id": 2,
    "txt": "two",
  },
  Object {
    "id": 3,
    "txt": "three",
  },
]
`

exports[`test/statement/clone.js TAP join clones the previous statement > must match snapshot 5`] = `
Array [
  Object {
    "id": 1,
    "join_junction": Array [
      Object {
        "ettin_id": 1,
        "parent_id": 1,
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_junction": Array [
      Object {
        "ettin_id": 2,
        "parent_id": 2,
      },
    ],
    "txt": "two",
  },
  Object {
    "id": 3,
    "join_junction": Array [
      Object {
        "ettin_id": 3,
        "parent_id": 3,
      },
    ],
    "txt": "three",
  },
]
`

exports[`test/statement/clone.js TAP join clones the previous statement > must match snapshot 6`] = `
Array [
  Object {
    "id": 1,
    "join_junction": Array [
      Object {
        "ettin_id": 1,
        "join_ettin": Array [
          Object {
            "ettin_id": null,
            "id": 1,
            "txt": "eerht",
          },
        ],
        "parent_id": 1,
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_junction": Array [
      Object {
        "ettin_id": 2,
        "join_ettin": Array [
          Object {
            "ettin_id": 1,
            "id": 2,
            "txt": "owt",
          },
        ],
        "parent_id": 2,
      },
    ],
    "txt": "two",
  },
  Object {
    "id": 3,
    "join_junction": Array [
      Object {
        "ettin_id": 3,
        "join_ettin": Array [
          Object {
            "ettin_id": 1,
            "id": 3,
            "txt": "eno",
          },
        ],
        "parent_id": 3,
      },
    ],
    "txt": "three",
  },
]
`

exports[`test/statement/clone.js TAP limit clones the previous statement > must match snapshot 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table"
`

exports[`test/statement/clone.js TAP limit clones the previous statement > must match snapshot 2`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" limit 4
`

exports[`test/statement/clone.js TAP limit clones the previous statement > must match snapshot 3`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" limit 2
`

exports[`test/statement/clone.js TAP limit clones the previous statement > must match snapshot 4`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP limit clones the previous statement > must match snapshot 5`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
]
`

exports[`test/statement/clone.js TAP limit clones the previous statement > must match snapshot 6`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/clone.js TAP offset clones the previous statement > must match snapshot 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table"
`

exports[`test/statement/clone.js TAP offset clones the previous statement > must match snapshot 2`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" offset 4
`

exports[`test/statement/clone.js TAP offset clones the previous statement > must match snapshot 3`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" offset 2
`

exports[`test/statement/clone.js TAP offset clones the previous statement > must match snapshot 4`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP offset clones the previous statement > must match snapshot 5`] = `
Array [
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP offset clones the previous statement > must match snapshot 6`] = `
Array [
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP order clones the previous statement > must match snapshot 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table"
`

exports[`test/statement/clone.js TAP order clones the previous statement > must match snapshot 2`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by id desc
`

exports[`test/statement/clone.js TAP order clones the previous statement > must match snapshot 3`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" order by id asc
`

exports[`test/statement/clone.js TAP order clones the previous statement > must match snapshot 4`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP order clones the previous statement > must match snapshot 5`] = `
Array [
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/statement/clone.js TAP order clones the previous statement > must match snapshot 6`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP project clones the previous statement > must match snapshot 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table"
`

exports[`test/statement/clone.js TAP project clones the previous statement > must match snapshot 2`] = `
select "regular_table"."id" as "id", "regular_table"."num" as "num", 1 as "one" from "regular_table"
`

exports[`test/statement/clone.js TAP project clones the previous statement > must match snapshot 3`] = `
select "regular_table"."id" as "id", "regular_table"."txt" as "txt", 2 as "two" from "regular_table"
`

exports[`test/statement/clone.js TAP project clones the previous statement > must match snapshot 4`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/clone.js TAP project clones the previous statement > must match snapshot 5`] = `
Array [
  Object {
    "id": 1,
    "num": 1,
    "one": 1,
  },
  Object {
    "id": 2,
    "num": 2.1,
    "one": 1,
  },
  Object {
    "id": 3,
    "num": 3.2,
    "one": 1,
  },
  Object {
    "id": 4,
    "num": 4.3,
    "one": 1,
  },
  Object {
    "id": 5,
    "num": 5.4,
    "one": 1,
  },
]
`

exports[`test/statement/clone.js TAP project clones the previous statement > must match snapshot 6`] = `
Array [
  Object {
    "id": 1,
    "two": 2,
    "txt": "one",
  },
  Object {
    "id": 2,
    "two": 2,
    "txt": "two",
  },
  Object {
    "id": 3,
    "two": 2,
    "txt": "three",
  },
  Object {
    "id": 4,
    "two": 2,
    "txt": "four",
  },
  Object {
    "id": 5,
    "two": 2,
    "txt": "five",
  },
]
`
