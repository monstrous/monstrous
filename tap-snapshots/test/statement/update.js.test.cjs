/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/update.js TAP updates and returns a record > matches query text 1`] = `
update "regular_table" set "bool" = $1, "num" = $2, "txt" = $3 where "regular_table"."id" = $4 returning "regular_table".*
`

exports[`test/statement/update.js TAP updates and returns a record > regular_table matches results 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 123.45,
    "txt": "text",
  },
]
`

exports[`test/statement/update.js TAP updates and returns a record as an object > matches query text 1`] = `
update "regular_table" set "bool" = $1, "num" = $2, "txt" = $3 where "regular_table"."id" = $4 returning "regular_table".*
`

exports[`test/statement/update.js TAP updates and returns a record as an object > regular_table matches results 1`] = `
Object {
  "bool": true,
  "id": 1,
  "inserted_at": {time},
  "num": 5.4321,
  "txt": "more",
}
`

exports[`test/statement/update.js TAP updates and returns multiple records > matches query text 1`] = `
update "regular_table" set "bool" = $1, "num" = $2, "txt" = $3 where bool is $4 returning "regular_table".*
`

exports[`test/statement/update.js TAP updates and returns multiple records > regular_table matches results 1`] = `
Array [
  Object {
    "bool": false,
    "id": 3,
    "inserted_at": {time},
    "num": 678.9,
    "txt": "two",
  },
  Object {
    "bool": false,
    "id": 5,
    "inserted_at": {time},
    "num": 678.9,
    "txt": "two",
  },
  Object {
    "bool": false,
    "id": 1,
    "inserted_at": {time},
    "num": 678.9,
    "txt": "two",
  },
]
`

exports[`test/statement/update.js TAP updates composite types and domains with pgp.as.format, exprs, or tuples > has_composite matches results 1`] = `
Array [
  Object {
    "first": "(-3,negative)",
    "id": 1,
    "second": "(3,positive)",
    "third": "(4,\\"also positive\\")",
  },
]
`

exports[`test/statement/update.js TAP updates composite types and domains with pgp.as.format, exprs, or tuples > matches query text 1`] = `
update "has_composite" set "first" = $1, "second" = (3, 'positive'), "third" = $2 where "has_composite"."id" = $3 returning "has_composite".*
`
