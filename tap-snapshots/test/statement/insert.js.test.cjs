/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/insert.js TAP ignores superfluous fields > matches query text 1`] = `
insert into "regular_table" (bool, num, txt) values ($1, $2, $3) returning *
`

exports[`test/statement/insert.js TAP ignores superfluous fields > regular_table matches results 1`] = `
Array [
  Object {
    "bool": true,
    "id": 12,
    "inserted_at": {time},
    "num": 123.45,
    "txt": "text",
  },
]
`

exports[`test/statement/insert.js TAP inserts and returns a record > matches query text 1`] = `
insert into "regular_table" (bool, num, txt) values ($1, $2, $3) returning *
`

exports[`test/statement/insert.js TAP inserts and returns a record > regular_table matches results 1`] = `
Array [
  Object {
    "bool": true,
    "id": 6,
    "inserted_at": {time},
    "num": 123.45,
    "txt": "text",
  },
]
`

exports[`test/statement/insert.js TAP inserts and returns a record as an object > matches query text 1`] = `
insert into "regular_table" (bool, num, txt) values ($1, $2, $3) returning *
`

exports[`test/statement/insert.js TAP inserts and returns a record as an object > regular_table matches results 1`] = `
Object {
  "bool": true,
  "id": 7,
  "inserted_at": {time},
  "num": 5.4321,
  "txt": "more",
}
`

exports[`test/statement/insert.js TAP inserts and returns multiple records > matches query text 1`] = `
insert into "regular_table" (bool, num, txt) values ($1, $2, $3), ($4, $5, $6) returning *
`

exports[`test/statement/insert.js TAP inserts and returns multiple records > regular_table matches results 1`] = `
Array [
  Object {
    "bool": true,
    "id": 8,
    "inserted_at": {time},
    "num": 123.45,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 9,
    "inserted_at": {time},
    "num": 678.9,
    "txt": "two",
  },
]
`

exports[`test/statement/insert.js TAP inserts composite types and domains with pgp.as.format, exprs, or tuples > has_composite matches results 1`] = `
Array [
  Object {
    "first": "(-1,negative)",
    "id": 3,
    "second": "(2,positive)",
    "third": "(3,\\"also positive\\")",
  },
]
`

exports[`test/statement/insert.js TAP inserts composite types and domains with pgp.as.format, exprs, or tuples > matches query text 1`] = `
insert into "has_composite" (first, second, third) values ($1, (2, 'positive'), $2) returning *
`

exports[`test/statement/insert.js TAP resolves inserting nothing > insert-nothing matches results 1`] = `
Array []
`

exports[`test/statement/insert.js TAP resolves inserting nothing > matches query text 1`] = `
Array []
`

exports[`test/statement/insert.js TAP sets columns based only on the first value > matches query text 1`] = `
insert into "regular_table" (num, txt) values ($1, $2), ($3, $4) returning *
`

exports[`test/statement/insert.js TAP sets columns based only on the first value > regular_table matches results 1`] = `
Array [
  Object {
    "bool": null,
    "id": 10,
    "inserted_at": {time},
    "num": 123.45,
    "txt": "one",
  },
  Object {
    "bool": null,
    "id": 11,
    "inserted_at": {time},
    "num": null,
    "txt": null,
  },
]
`

exports[`test/statement/insert.js TAP uses qualified fields > matches query text 1`] = `
insert into "regular_table" (bool, num, txt) values ($1, $2, $3) returning *
`

exports[`test/statement/insert.js TAP uses qualified fields > regular_table matches results 1`] = `
Array [
  Object {
    "bool": null,
    "id": 13,
    "inserted_at": {time},
    "num": null,
    "txt": null,
  },
]
`
