/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/delete.js TAP delete-joins > alpha matches results 1`] = `
Array [
  Object {
    "alpha_id_one": 2,
    "alpha_id_two": null,
    "beta_id": 3,
    "id": 4,
    "j": null,
    "val": "alpha two (alpha null) beta three",
  },
  Object {
    "alpha_id_one": 3,
    "alpha_id_two": 1,
    "beta_id": 4,
    "id": 5,
    "j": null,
    "val": "alpha three alpha one beta four",
  },
]
`

exports[`test/join/delete.js TAP delete-joins > matches query text 1`] = `
delete from "foreign_keys"."gamma" using "foreign_keys"."beta" inner join "foreign_keys"."alpha" on "foreign_keys"."alpha"."id" = "foreign_keys"."beta"."alpha_id" where "foreign_keys"."gamma"."beta_id" = "foreign_keys"."beta"."id" and "foreign_keys"."alpha"."id" = $1 returning "foreign_keys"."gamma".*
`

exports[`test/join/delete.js TAP delete-joins all rows in the origin > alpha matches results 1`] = `
Array [
  Object {
    "alpha_id_one": 1,
    "alpha_id_two": 1,
    "beta_id": 1,
    "id": 1,
    "j": null,
    "val": "alpha one alpha one beta one",
  },
  Object {
    "alpha_id_one": 1,
    "alpha_id_two": 2,
    "beta_id": 2,
    "id": 2,
    "j": null,
    "val": "alpha two alpha two beta two",
  },
  Object {
    "alpha_id_one": 2,
    "alpha_id_two": 3,
    "beta_id": 2,
    "id": 3,
    "j": null,
    "val": "alpha two alpha three beta two again",
  },
]
`

exports[`test/join/delete.js TAP delete-joins all rows in the origin > matches query text 1`] = `
delete from "foreign_keys"."gamma" using "foreign_keys"."beta" inner join "foreign_keys"."alpha" on "foreign_keys"."alpha"."id" = "foreign_keys"."beta"."alpha_id" where "foreign_keys"."gamma"."beta_id" = "foreign_keys"."beta"."id" returning "foreign_keys"."gamma".*
`
