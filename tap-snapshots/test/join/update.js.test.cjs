/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/update.js TAP ignores non-column keys > alpha matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "updated",
  },
]
`

exports[`test/join/update.js TAP ignores non-column keys > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = $1 from "foreign_keys"."beta" where "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" and "foreign_keys"."alpha"."id" = $2 returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP updates all rows in the origin > alpha matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "updated",
  },
  Object {
    "id": 2,
    "val": "updated",
  },
  Object {
    "id": 3,
    "val": "updated",
  },
]
`

exports[`test/join/update.js TAP updates all rows in the origin > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = $1 from "foreign_keys"."beta" where "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP updates the origin of a deeply nested join > alpha matches results 1`] = `
Array [
  Object {
    "id": 2,
    "val": "also also also updated",
  },
]
`

exports[`test/join/update.js TAP updates the origin of a deeply nested join > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = $1 from "foreign_keys"."beta" inner join "foreign_keys"."gamma" on "foreign_keys"."gamma"."beta_id" = "foreign_keys"."beta"."id" where "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" and "foreign_keys"."gamma"."id" = $2 returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP updates the origin of a join based on dependent criteria > alpha matches results 1`] = `
Array [
  Object {
    "id": 2,
    "val": "also updated",
  },
  Object {
    "id": 3,
    "val": "also updated",
  },
]
`

exports[`test/join/update.js TAP updates the origin of a join based on dependent criteria > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = $1 from "foreign_keys"."gamma" where "foreign_keys"."alpha"."id" = "foreign_keys"."gamma"."alpha_id_two" and "foreign_keys"."gamma"."beta_id" = $2 returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP updates the origin of a join based on origin criteria > alpha matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "updated",
  },
]
`

exports[`test/join/update.js TAP updates the origin of a join based on origin criteria > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = $1 from "foreign_keys"."beta" where "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" and "foreign_keys"."alpha"."id" = $2 returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP updates the origin of a join to a dependent value with constant criteria > alpha matches results 1`] = `
Array [
  Object {
    "id": 2,
    "val": "alpha one",
  },
]
`

exports[`test/join/update.js TAP updates the origin of a join to a dependent value with constant criteria > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = "foreign_keys"."beta"."val" from "foreign_keys"."beta" where "foreign_keys"."beta"."id" = $1 and "foreign_keys"."alpha"."id" = $2 returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP updates the origin of a join with an expr > alpha matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "updated with an expr!",
  },
]
`

exports[`test/join/update.js TAP updates the origin of a join with an expr > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = "foreign_keys"."alpha"."val" || ' with an ' || 'expr!' from "foreign_keys"."beta" where "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" and "foreign_keys"."alpha"."id" = $1 returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP updates the origin of a join with constant criteria > alpha matches results 1`] = `
Array [
  Object {
    "id": 2,
    "val": "also also updated",
  },
]
`

exports[`test/join/update.js TAP updates the origin of a join with constant criteria > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = $1 from "foreign_keys"."beta" where "foreign_keys"."beta"."id" = $2 and "foreign_keys"."alpha"."id" = $3 returning "foreign_keys"."alpha".*
`

exports[`test/join/update.js TAP uses qualified keys > alpha matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "updated",
  },
]
`

exports[`test/join/update.js TAP uses qualified keys > matches query text 1`] = `
update "foreign_keys"."alpha" set "val" = $1 from "foreign_keys"."beta" where "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" and "foreign_keys"."alpha"."id" = $2 returning "foreign_keys"."alpha".*
`
