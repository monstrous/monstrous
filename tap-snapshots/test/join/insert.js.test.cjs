/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/insert.js TAP allows objects > beta and delta match results 1`] = `
Array [
  Object {
    "alpha_id": 9,
    "delta": Array [
      Object {
        "beta_id": 14,
        "id": 5,
        "val": "in delta",
      },
    ],
    "id": 14,
    "j": null,
    "val": "in beta",
  },
]
`

exports[`test/join/insert.js TAP allows objects > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2) ) as beta_values (val) cross join alpha_cte returning * ) , delta_cte as ( insert into "foreign_keys_too"."delta" ("beta_id", "val") select beta_cte."id", delta_values.val from (values ($3) ) as delta_values (val) cross join beta_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP allows objects > matches results 1`] = `
Array [
  Object {
    "id": 9,
    "val": "novel and refined",
  },
]
`

exports[`test/join/insert.js TAP expresses recursive chains with flattened values > beta and delta match results 1`] = `
Array [
  Object {
    "alpha_id": 10,
    "delta": Array [
      Object {
        "beta_id": 15,
        "id": 6,
        "val": "in delta",
      },
      Object {
        "beta_id": 15,
        "id": 7,
        "val": "in delta again",
      },
    ],
    "id": 15,
    "j": null,
    "val": "in beta",
  },
]
`

exports[`test/join/insert.js TAP expresses recursive chains with flattened values > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2) ) as beta_values (val) cross join alpha_cte returning * ) , delta_cte as ( insert into "foreign_keys_too"."delta" ("beta_id", "val") select beta_cte."id", delta_values.val from (values ($3), ($4) ) as delta_values (val) cross join beta_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP expresses recursive chains with flattened values > matches results 1`] = `
Array [
  Object {
    "id": 10,
    "val": "current and enriched",
  },
]
`

exports[`test/join/insert.js TAP ignores superfluous fields deeper in the join > alpha matches results 1`] = `
Array [
  Object {
    "id": 13,
    "val": "new and improved",
  },
]
`

exports[`test/join/insert.js TAP ignores superfluous fields deeper in the join > beta matches results 1`] = `
Array [
  Object {
    "alpha_id": 13,
    "id": 20,
    "j": null,
    "val": "in beta",
  },
  Object {
    "alpha_id": 13,
    "id": 21,
    "j": null,
    "val": "in beta too",
  },
]
`

exports[`test/join/insert.js TAP ignores superfluous fields deeper in the join > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2), ($3) ) as beta_values (val) cross join alpha_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP ignores superfluous fields in root relation > alpha matches results 1`] = `
Array [
  Object {
    "id": 12,
    "val": "new and improved",
  },
]
`

exports[`test/join/insert.js TAP ignores superfluous fields in root relation > beta matches results 1`] = `
Array [
  Object {
    "alpha_id": 12,
    "id": 18,
    "j": null,
    "val": "in beta",
  },
  Object {
    "alpha_id": 12,
    "id": 19,
    "j": null,
    "val": "in beta too",
  },
]
`

exports[`test/join/insert.js TAP ignores superfluous fields in root relation > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2), ($3) ) as beta_values (val) cross join alpha_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP inserts longer recursive chains > beta and delta match results 1`] = `
Array [
  Object {
    "alpha_id": 8,
    "delta": Array [
      Object {
        "beta_id": 13,
        "id": 3,
        "val": "in delta",
      },
      Object {
        "beta_id": 13,
        "id": 4,
        "val": "in delta again",
      },
    ],
    "id": 13,
    "j": null,
    "val": "in beta",
  },
]
`

exports[`test/join/insert.js TAP inserts longer recursive chains > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2) ) as beta_values (val) cross join alpha_cte returning * ) , delta_cte as ( insert into "foreign_keys_too"."delta" ("beta_id", "val") select beta_cte."id", delta_values.val from (values ($3), ($4) ) as delta_values (val) cross join beta_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP inserts longer recursive chains > matches results 1`] = `
Array [
  Object {
    "id": 8,
    "val": "novel and refined",
  },
]
`

exports[`test/join/insert.js TAP inserts multiple dependents > beta matches results 1`] = `
Array [
  Object {
    "alpha_id": 7,
    "id": 11,
    "j": null,
    "val": "in beta",
  },
  Object {
    "alpha_id": 7,
    "id": 12,
    "j": null,
    "val": "in beta too",
  },
]
`

exports[`test/join/insert.js TAP inserts multiple dependents > epsilon matches results 1`] = `
Array [
  Object {
    "alpha_id": 7,
    "id": 3,
    "val": "in epsilon",
  },
  Object {
    "alpha_id": 7,
    "id": 4,
    "val": "in epsilon again",
  },
]
`

exports[`test/join/insert.js TAP inserts multiple dependents > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2), ($3) ) as beta_values (val) cross join alpha_cte returning * ) , epsilon_cte as ( insert into "foreign_keys_too"."epsilon" ("alpha_id", "val") select alpha_cte."id", epsilon_values.val from (values ($4), ($5) ) as epsilon_values (val) cross join alpha_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP inserts multiple dependents > matches results 1`] = `
Array [
  Object {
    "id": 7,
    "val": "newest and improvedest",
  },
]
`

exports[`test/join/insert.js TAP inserts the origin of a join with an expr > child matches results 1`] = `
Array [
  Object {
    "id": "{uuid}",
    "parent_id": "{uuid}",
    "uu": null,
    "val": "{now} with another expr!",
  },
]
`

exports[`test/join/insert.js TAP inserts the origin of a join with an expr > matches query text 1`] = `
with uuid_parent_cte as (insert into "foreign_keys"."uuid_parent" (val) values (now()::text || ' with an ' || 'expr!') returning *) , child_cte as ( insert into "foreign_keys"."uuid_child" ("parent_id", "val") select uuid_parent_cte."id", child_values.val from (values (now()::text || ' with another ' || 'expr!') ) as child_values (val) cross join uuid_parent_cte returning * ) select * from uuid_parent_cte
`

exports[`test/join/insert.js TAP inserts the origin of a join with an expr > parent matches results 1`] = `
Array [
  Object {
    "id": "{uuid}",
    "uu": null,
    "val": "{now} with an expr!",
  },
]
`

exports[`test/join/insert.js TAP inserts the origin of a table-only join > alpha matches results 1`] = `
Array [
  Object {
    "id": 5,
    "val": "new and improved",
  },
]
`

exports[`test/join/insert.js TAP inserts the origin of a table-only join > beta matches results 1`] = `
Array [
  Object {
    "alpha_id": 5,
    "id": 7,
    "j": null,
    "val": "in beta",
  },
  Object {
    "alpha_id": 5,
    "id": 8,
    "j": null,
    "val": "in beta too",
  },
]
`

exports[`test/join/insert.js TAP inserts the origin of a table-only join > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2), ($3) ) as beta_values (val) cross join alpha_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP inserts the origin of a table-only join over uuids, with more uuids > child matches results 1`] = `
Array [
  Object {
    "id": "{uuid}",
    "parent_id": "{uuid}",
    "uu": "{uuid}",
    "val": "in child",
  },
  Object {
    "id": "{uuid}",
    "parent_id": "{uuid}",
    "uu": "{uuid}",
    "val": "in child too",
  },
]
`

exports[`test/join/insert.js TAP inserts the origin of a table-only join over uuids, with more uuids > matches query text 1`] = `
with uuid_parent_cte as (insert into "foreign_keys"."uuid_parent" (val, uu) values ($1, $2) returning *) , child_cte as ( insert into "foreign_keys"."uuid_child" ("parent_id", "val", "uu") select uuid_parent_cte."id", child_values.val, child_values.uu from (values ($3, $4::uuid), ($5, $6::uuid) ) as child_values (val, uu) cross join uuid_parent_cte returning * ) select * from uuid_parent_cte
`

exports[`test/join/insert.js TAP inserts the origin of a table-only join over uuids, with more uuids > parent matches results 1`] = `
Array [
  Object {
    "id": "{uuid}",
    "uu": "{uuid}",
    "val": "new and improved",
  },
]
`

exports[`test/join/insert.js TAP uses fully-qualified names > alpha matches results 1`] = `
Array [
  Object {
    "id": 6,
    "val": "newer and improveder",
  },
]
`

exports[`test/join/insert.js TAP uses fully-qualified names > beta matches results 1`] = `
Array [
  Object {
    "alpha_id": 6,
    "id": 9,
    "j": null,
    "val": "in beta",
  },
  Object {
    "alpha_id": 6,
    "id": 10,
    "j": null,
    "val": "in beta too",
  },
]
`

exports[`test/join/insert.js TAP uses fully-qualified names > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , beta_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", beta_values.val from (values ($2), ($3) ) as beta_values (val) cross join alpha_cte returning * ) select * from alpha_cte
`

exports[`test/join/insert.js TAP works with aliased non-root relations > alpha matches results 1`] = `
Array [
  Object {
    "id": 11,
    "val": "new and improved",
  },
]
`

exports[`test/join/insert.js TAP works with aliased non-root relations > beta as b matches results 1`] = `
Array [
  Object {
    "alpha_id": 11,
    "id": 16,
    "j": null,
    "val": "in b",
  },
  Object {
    "alpha_id": 11,
    "id": 17,
    "j": null,
    "val": "in b too",
  },
]
`

exports[`test/join/insert.js TAP works with aliased non-root relations > matches query text 1`] = `
with alpha_cte as (insert into "foreign_keys"."alpha" (val) values ($1) returning *) , b_cte as ( insert into "foreign_keys"."beta" ("alpha_id", "val") select alpha_cte."id", b_values.val from (values ($2), ($3) ) as b_values (val) cross join alpha_cte returning * ) select * from alpha_cte
`
