/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/constants.js TAP catches constants that start with valid keys > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."alpha_id" = "foreign_keys"."alpha"."id" and "foreign_keys_too"."epsilon"."val" <> $1
`

exports[`test/join/constants.js TAP catches constants that start with valid keys > matches results 1`] = `
Array [
  Object {
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/constants.js TAP handles simple operations > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."alpha_id" is $1 where "foreign_keys"."alpha"."val" = $2
`

exports[`test/join/constants.js TAP handles simple operations > matches results 1`] = `
Array [
  Object {
    "epsilon": Array [
      Object {
        "alpha_id": null,
        "id": 2,
        "val": "not two",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/constants.js TAP joins on constants > matches query text 1`] = `
select "foreign_keys"."beta"."id" as "id", "foreign_keys"."beta"."alpha_id" as "alpha_id", "foreign_keys"."beta"."val" as "val", "foreign_keys"."beta"."j" as "j", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."beta" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."val" = $1 where "foreign_keys"."beta"."val" = $2
`

exports[`test/join/constants.js TAP joins on constants > matches results 1`] = `
Array [
  Object {
    "alpha_id": 3,
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 4,
    "j": null,
    "val": "alpha three again",
  },
]
`

exports[`test/join/constants.js TAP joins on constants for multiple relations > matches query text 1`] = `
select "foreign_keys"."beta"."id" as "id", "foreign_keys"."beta"."alpha_id" as "alpha_id", "foreign_keys"."beta"."val" as "val", "foreign_keys"."beta"."j" as "j", "foreign_keys"."alpha"."id" as "alpha__id", "foreign_keys"."alpha"."val" as "alpha__val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."beta" inner join "foreign_keys"."alpha" on "foreign_keys"."alpha"."val" = $1 inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."id" = $2 and "foreign_keys_too"."epsilon"."val" = $3 where "foreign_keys"."beta"."val" = $4
`

exports[`test/join/constants.js TAP joins on constants for multiple relations > matches results 1`] = `
Array [
  Object {
    "alpha": Array [
      Object {
        "id": 1,
        "val": "one",
      },
    ],
    "alpha_id": 3,
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 4,
    "j": null,
    "val": "alpha three again",
  },
]
`

exports[`test/join/constants.js TAP joins on multiple constants > matches query text 1`] = `
select "foreign_keys"."beta"."id" as "id", "foreign_keys"."beta"."alpha_id" as "alpha_id", "foreign_keys"."beta"."val" as "val", "foreign_keys"."beta"."j" as "j", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."beta" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."id" = $1 and "foreign_keys_too"."epsilon"."val" = $2 where "foreign_keys"."beta"."val" = $3
`

exports[`test/join/constants.js TAP joins on multiple constants > matches results 1`] = `
Array [
  Object {
    "alpha_id": 3,
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 4,
    "j": null,
    "val": "alpha three again",
  },
]
`

exports[`test/join/constants.js TAP mixes keys and constants > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."alpha_id" = "foreign_keys"."alpha"."id" and "foreign_keys_too"."epsilon"."val" = $1
`

exports[`test/join/constants.js TAP mixes keys and constants > matches results 1`] = `
Array [
  Object {
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`
