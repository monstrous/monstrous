/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/values.js TAP joins a values list > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "txt": "one",
    "v": Object {
      "a": 1,
      "b": 2,
    },
  },
  Object {
    "id": 3,
    "txt": "three",
    "v": Object {
      "a": 3,
      "b": 4,
    },
  },
]
`

exports[`test/join/values.js TAP joins a values list > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "v"."a" as "v__a", "v"."b" as "v__b" from "join_parent" inner join (values ($1, $2), ($3, $4)) as v ("a", "b") on "v".a = "join_parent"."id"
`

exports[`test/join/values.js TAP joins a values list, inline > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "txt": "one",
    "v": Object {
      "a": 1,
      "b": 2,
    },
  },
  Object {
    "id": 3,
    "txt": "three",
    "v": Object {
      "a": 3,
      "b": 4,
    },
  },
]
`

exports[`test/join/values.js TAP joins a values list, inline > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "v"."a" as "v__a", "v"."b" as "v__b" from "join_parent" inner join (values ($1, $2), ($3, $4)) as v ("a", "b") on v.a = "join_parent"."id"
`
