/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/count.js TAP aliases count > matches query text 1`] = `
select count(distinct beta.id) as "total" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/count.js TAP aliases count > matches results 1`] = `
Array [
  Object {
    "total": "3",
  },
]
`

exports[`test/join/count.js TAP counts > matches query text 1`] = `
select count(*) as "count" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/count.js TAP counts > matches results 1`] = `
Array [
  Object {
    "count": "3",
  },
]
`

exports[`test/join/count.js TAP counts an expr > matches query text 1`] = `
select count(distinct beta.id) as "count" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/count.js TAP counts an expr > matches results 1`] = `
Array [
  Object {
    "count": "3",
  },
]
`
