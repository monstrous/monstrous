/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/index.js TAP basic join > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
      Object {
        "id": 3,
        "parent_id": 2,
        "txt": "two-two",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP basic join > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on "join_child"."parent_id" = "join_parent"."id"
`

exports[`test/join/index.js TAP basic join, on child to parent > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
      Object {
        "id": 3,
        "parent_id": 2,
        "txt": "two-two",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP basic join, on child to parent > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on "join_child"."parent_id" = "join_parent"."id"
`

exports[`test/join/index.js TAP basic join, on parent to child > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
      Object {
        "id": 3,
        "parent_id": 2,
        "txt": "two-two",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP basic join, on parent to child > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on "join_parent"."id" = "join_child"."parent_id"
`

exports[`test/join/index.js TAP basic join, returning nothing > joins parent and child tables 1`] = `
Array []
`

exports[`test/join/index.js TAP basic join, returning nothing > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on parent_id = "join_parent"."id" where "join_parent"."txt" = $1
`

exports[`test/join/index.js TAP basic join, unqualified key > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
      Object {
        "id": 3,
        "parent_id": 2,
        "txt": "two-two",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP basic join, unqualified key > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on parent_id = "join_parent"."id"
`

exports[`test/join/index.js TAP can join on any fields > matches query text 1`] = `
select "foreign_keys"."beta"."id" as "id", "foreign_keys"."beta"."alpha_id" as "alpha_id", "foreign_keys"."beta"."val" as "val", "foreign_keys"."beta"."j" as "j", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."beta" inner join "foreign_keys_too"."epsilon" on "foreign_keys"."beta"."val" = "foreign_keys_too"."epsilon"."val"
`

exports[`test/join/index.js TAP can join on any fields > matches results 1`] = `
Array [
  Object {
    "alpha_id": 1,
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "j": null,
    "val": "alpha one",
  },
]
`

exports[`test/join/index.js TAP can join with operation > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" <> "foreign_keys"."alpha"."id"
`

exports[`test/join/index.js TAP can join with operation > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 2,
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 1,
    "val": "one",
  },
  Object {
    "beta": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "j": null,
        "val": "alpha one",
      },
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 2,
    "val": "two",
  },
  Object {
    "beta": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "j": null,
        "val": "alpha one",
      },
      Object {
        "alpha_id": 2,
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
    ],
    "id": 3,
    "val": "three",
  },
  Object {
    "beta": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "j": null,
        "val": "alpha one",
      },
      Object {
        "alpha_id": 2,
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 4,
    "val": "four",
  },
]
`

exports[`test/join/index.js TAP changes join types > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j" from "foreign_keys"."alpha" left outer join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" > $1
`

exports[`test/join/index.js TAP changes join types > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 2,
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
    ],
    "id": 2,
    "val": "two",
  },
  Object {
    "beta": Array [
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
  Object {
    "beta": Array [],
    "id": 4,
    "val": "four",
  },
]
`

exports[`test/join/index.js TAP foreign keys from parent, referent aliased > joins parent and child tables, with child aliased 1`] = `
Array [
  Object {
    "children": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "id": 1,
    "txt": "one",
  },
  Object {
    "children": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
      Object {
        "id": 3,
        "parent_id": 2,
        "txt": "two-two",
      },
    ],
    "id": 2,
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP foreign keys from parent, referent aliased > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "children"."id" as "children__id", "children"."parent_id" as "children__parent_id", "children"."txt" as "children__txt" from "join_parent" inner join "join_child" as "children" on "children"."parent_id" = "join_parent"."id"
`

exports[`test/join/index.js TAP foreign keys from referent, parent aliased > joins child and parent tables, with parent aliased 1`] = `
Array [
  Object {
    "id": 1,
    "par": Array [
      Object {
        "id": 1,
        "txt": "one",
      },
    ],
    "parent_id": 1,
    "txt": "one-one",
  },
  Object {
    "id": 2,
    "par": Array [
      Object {
        "id": 2,
        "txt": "two",
      },
    ],
    "parent_id": 2,
    "txt": "two-one",
  },
  Object {
    "id": 3,
    "par": Array [
      Object {
        "id": 2,
        "txt": "two",
      },
    ],
    "parent_id": 2,
    "txt": "two-two",
  },
]
`

exports[`test/join/index.js TAP foreign keys from referent, parent aliased > matches query text 1`] = `
select "join_child"."id" as "id", "join_child"."parent_id" as "parent_id", "join_child"."txt" as "txt", "par"."id" as "par__id", "par"."txt" as "par__txt" from "join_child" inner join "join_parent" as "par" on "join_child"."parent_id" = "par"."id"
`

exports[`test/join/index.js TAP joins a relation in another schema > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."alpha_id" = "foreign_keys"."alpha"."id"
`

exports[`test/join/index.js TAP joins a relation in another schema > matches results 1`] = `
Array [
  Object {
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/index.js TAP joins a relation in another schema through an implicit foreign key relationship > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."alpha_id" = "foreign_keys"."alpha"."id"
`

exports[`test/join/index.js TAP joins a relation in another schema through an implicit foreign key relationship > matches results 1`] = `
Array [
  Object {
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/index.js TAP joins on TRUE > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys_too"."epsilon" on true where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/index.js TAP joins on TRUE > matches results 1`] = `
Array [
  Object {
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
      Object {
        "alpha_id": null,
        "id": 2,
        "val": "not two",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/index.js TAP many:many join > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_junction": Array [
      Object {
        "ettin_id": 1,
        "join_ettin": Array [
          Object {
            "ettin_id": null,
            "id": 1,
            "txt": "eerht",
          },
        ],
        "parent_id": 1,
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_junction": Array [
      Object {
        "ettin_id": 2,
        "join_ettin": Array [
          Object {
            "ettin_id": 1,
            "id": 2,
            "txt": "owt",
          },
        ],
        "parent_id": 2,
      },
    ],
    "txt": "two",
  },
  Object {
    "id": 3,
    "join_junction": Array [
      Object {
        "ettin_id": 3,
        "join_ettin": Array [
          Object {
            "ettin_id": 1,
            "id": 3,
            "txt": "eno",
          },
        ],
        "parent_id": 3,
      },
    ],
    "txt": "three",
  },
]
`

exports[`test/join/index.js TAP many:many join > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_junction"."parent_id" as "join_junction__parent_id", "join_junction"."ettin_id" as "join_junction__ettin_id", "join_ettin"."id" as "join_junction__join_ettin__id", "join_ettin"."ettin_id" as "join_junction__join_ettin__ettin_id", "join_ettin"."txt" as "join_junction__join_ettin__txt" from "join_parent" inner join "join_junction" on "join_junction"."parent_id" = "join_parent"."id" inner join "join_ettin" on "join_junction"."ettin_id" = "join_ettin"."id"
`

exports[`test/join/index.js TAP using foreign keys from the parent > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
      Object {
        "id": 3,
        "parent_id": 2,
        "txt": "two-two",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP using foreign keys from the parent > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on "join_child"."parent_id" = "join_parent"."id"
`

exports[`test/join/index.js TAP using foreign keys from the referent > joins child and parent tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_parent": Array [
      Object {
        "id": 1,
        "txt": "one",
      },
    ],
    "parent_id": 1,
    "txt": "one-one",
  },
  Object {
    "id": 2,
    "join_parent": Array [
      Object {
        "id": 2,
        "txt": "two",
      },
    ],
    "parent_id": 2,
    "txt": "two-one",
  },
  Object {
    "id": 3,
    "join_parent": Array [
      Object {
        "id": 2,
        "txt": "two",
      },
    ],
    "parent_id": 2,
    "txt": "two-two",
  },
]
`

exports[`test/join/index.js TAP using foreign keys from the referent > matches query text 1`] = `
select "join_child"."id" as "id", "join_child"."parent_id" as "parent_id", "join_child"."txt" as "txt", "join_parent"."id" as "join_parent__id", "join_parent"."txt" as "join_parent__txt" from "join_child" inner join "join_parent" on "join_child"."parent_id" = "join_parent"."id"
`

exports[`test/join/index.js TAP with expr in join condition > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "two",
  },
  Object {
    "id": 3,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "three",
  },
]
`

exports[`test/join/index.js TAP with expr in join condition > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on "join_child"."txt" ilike 'two-one'
`

exports[`test/join/index.js TAP with expr in join condition criteria > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "one",
  },
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "two",
  },
  Object {
    "id": 3,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "three",
  },
]
`

exports[`test/join/index.js TAP with expr in join condition criteria > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on "join_child"."txt" = 'two' || '-' || 'one'
`

exports[`test/join/index.js TAP with parameters in join condition, all columns qualified > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP with parameters in join condition, all columns qualified > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on "join_child"."parent_id" = "join_parent"."id" and "join_child"."txt" = $1
`

exports[`test/join/index.js TAP with parameters in join condition, ambiguous column qualified > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP with parameters in join condition, ambiguous column qualified > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on parent_id = "join_parent"."id" and "join_child"."txt" = $1
`

exports[`test/join/index.js TAP with parameters in join condition, relying on auto-association to child > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 2,
    "join_child": Array [
      Object {
        "id": 2,
        "parent_id": 2,
        "txt": "two-one",
      },
    ],
    "txt": "two",
  },
]
`

exports[`test/join/index.js TAP with parameters in join condition, relying on auto-association to child > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join "join_child" on parent_id = "join_parent"."id" and "join_child"."txt" = $1
`
