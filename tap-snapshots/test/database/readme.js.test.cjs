/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/database/readme.js TAP all together > matches result 1`] = `
Array [
  Object {
    "authors": Array [
      Object {
        "age": "79",
        "books": Array [
          Object {
            "author_id": 1,
            "id": 2,
            "print_date": null,
            "publisher": null,
            "title": "one",
          },
          Object {
            "author_id": 1,
            "id": 3,
            "print_date": null,
            "publisher": null,
            "title": "two",
          },
          Object {
            "author_id": 1,
            "id": 4,
            "print_date": null,
            "publisher": null,
            "title": "three",
          },
          Object {
            "author_id": 1,
            "id": 5,
            "print_date": null,
            "publisher": null,
            "title": "four",
          },
          Object {
            "author_id": 1,
            "id": 6,
            "print_date": null,
            "publisher": null,
            "title": "five",
          },
          Object {
            "author_id": 1,
            "id": 7,
            "print_date": null,
            "publisher": null,
            "title": "six",
          },
          Object {
            "author_id": 1,
            "id": 8,
            "print_date": null,
            "publisher": null,
            "title": "seven",
          },
          Object {
            "author_id": 1,
            "id": 9,
            "print_date": null,
            "publisher": null,
            "title": "eight",
          },
          Object {
            "author_id": 1,
            "id": 10,
            "print_date": null,
            "publisher": null,
            "title": "nine",
          },
          Object {
            "author_id": 1,
            "id": 11,
            "print_date": null,
            "publisher": null,
            "title": "ten",
          },
          Object {
            "author_id": 1,
            "id": 12,
            "print_date": null,
            "publisher": null,
            "title": "eleven",
          },
        ],
        "id": 1,
        "name": "Lauren Ipsum",
      },
    ],
    "founded": {time},
    "id": 1,
    "name": "carnegie 1",
    "postcode": "12345",
  },
]
`

exports[`test/database/readme.js TAP attachment > matches result 1`] = `
Array [
  Object {
    "founded": {time},
    "holdings": Array [
      Object {
        "book_id": 2,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 2,
            "print_date": null,
            "publisher": null,
            "title": "one",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 3,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 3,
            "print_date": null,
            "publisher": null,
            "title": "two",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 4,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 4,
            "print_date": null,
            "publisher": null,
            "title": "three",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 5,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 5,
            "print_date": null,
            "publisher": null,
            "title": "four",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 6,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 6,
            "print_date": null,
            "publisher": null,
            "title": "five",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 7,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 7,
            "print_date": null,
            "publisher": null,
            "title": "six",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 8,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 8,
            "print_date": null,
            "publisher": null,
            "title": "seven",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 9,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 9,
            "print_date": null,
            "publisher": null,
            "title": "eight",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 10,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 10,
            "print_date": null,
            "publisher": null,
            "title": "nine",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 11,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 11,
            "print_date": null,
            "publisher": null,
            "title": "ten",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 12,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 12,
            "print_date": null,
            "publisher": null,
            "title": "eleven",
          },
        ],
        "library_id": 1,
      },
    ],
    "id": 1,
    "name": "carnegie 1",
    "postcode": "12345",
  },
]
`

exports[`test/database/readme.js TAP fluency > matches result 1`] = `
Array [
  Object {
    "founded": {time},
    "holdings": Array [
      Object {
        "book_id": 2,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 2,
            "print_date": null,
            "publisher": null,
            "title": "one",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 3,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 3,
            "print_date": null,
            "publisher": null,
            "title": "two",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 4,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 4,
            "print_date": null,
            "publisher": null,
            "title": "three",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 5,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 5,
            "print_date": null,
            "publisher": null,
            "title": "four",
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 6,
        "books": Array [
          Object {
            "author_id": 1,
            "authors": Array [
              Object {
                "birth": {time},
                "death": {time},
                "id": 1,
                "is_in_print": false,
                "name": "Lauren Ipsum",
              },
            ],
            "id": 6,
            "print_date": null,
            "publisher": null,
            "title": "five",
          },
        ],
        "library_id": 1,
      },
    ],
    "id": 1,
    "name": "carnegie 1",
    "postcode": "12345",
  },
]
`

exports[`test/database/readme.js TAP persistence: delete > matches result 1`] = `
Array [
  Object {
    "book_id": 1,
    "library_id": 1,
  },
  Object {
    "book_id": 1,
    "library_id": 2,
  },
]
`

exports[`test/database/readme.js TAP persistence: insert > matches result 1`] = `
Array [
  Object {
    "birth": null,
    "death": null,
    "id": 1,
    "is_in_print": false,
    "name": "Lauren Ipsum",
  },
  Object {
    "birth": null,
    "death": null,
    "id": 2,
    "is_in_print": false,
    "name": "Daler S. Ahmet",
  },
]
`

exports[`test/database/readme.js TAP persistence: insert into a join > matches inserted 1`] = `
Array [
  Object {
    "author_id": 3,
    "holdings": Array [
      Object {
        "book_id": 1,
        "library_id": 1,
      },
      Object {
        "book_id": 1,
        "library_id": 2,
      },
    ],
    "id": 1,
    "print_date": {time},
    "publisher": "Aleatory Domicile",
    "title": "The Placeholder",
  },
]
`

exports[`test/database/readme.js TAP persistence: insert into a join > matches result 1`] = `
Object {
  "birth": null,
  "death": null,
  "id": 3,
  "is_in_print": false,
  "name": "Consuela Ctetur",
}
`

exports[`test/database/readme.js TAP persistence: insert subquery, also tasks > matches result 1`] = `
Array [
  Object {
    "book_id": 2,
    "library_id": 1,
  },
  Object {
    "book_id": 3,
    "library_id": 1,
  },
  Object {
    "book_id": 4,
    "library_id": 1,
  },
  Object {
    "book_id": 5,
    "library_id": 1,
  },
  Object {
    "book_id": 6,
    "library_id": 1,
  },
  Object {
    "book_id": 7,
    "library_id": 1,
  },
  Object {
    "book_id": 8,
    "library_id": 1,
  },
  Object {
    "book_id": 9,
    "library_id": 1,
  },
  Object {
    "book_id": 10,
    "library_id": 1,
  },
  Object {
    "book_id": 11,
    "library_id": 1,
  },
  Object {
    "book_id": 12,
    "library_id": 1,
  },
]
`

exports[`test/database/readme.js TAP persistence: save > matches result 1`] = `
Object {
  "birth": {time},
  "death": {time},
  "id": 1,
  "is_in_print": false,
  "name": "Lauren Ipsum",
}
`

exports[`test/database/readme.js TAP persistence: update with exprs > matches result 1`] = `
Array [
  Object {
    "birth": null,
    "death": null,
    "id": 3,
    "is_in_print": true,
    "name": "Consuela Ctetur",
  },
]
`

exports[`test/database/readme.js TAP raw sql: named > matches result 1`] = `
Object {
  "author_id": 3,
  "id": 1,
  "print_date": {time},
  "publisher": "Aleatory Domicile",
  "title": "The Placeholder",
}
`

exports[`test/database/readme.js TAP raw sql: ordinal > matches result 1`] = `
Array [
  Object {
    "author_id": 3,
    "id": 1,
    "print_date": {time},
    "publisher": "Aleatory Domicile",
    "title": "The Placeholder",
  },
]
`

exports[`test/database/readme.js TAP retrieval: aliasing > matches result 1`] = `
Array [
  Object {
    "bookplaces": Array [
      Object {
        "founded": {time},
        "id": 1,
        "name": "carnegie 1",
        "postcode": "12345",
      },
    ],
    "hired_on": {time},
    "id": 1,
    "library_id": 1,
    "name": "Adip Singh",
    "position": "director",
  },
  Object {
    "bookplaces": Array [
      Object {
        "founded": {time},
        "id": 1,
        "name": "carnegie 1",
        "postcode": "12345",
      },
    ],
    "hired_on": {time},
    "id": 2,
    "library_id": 1,
    "name": "Eli T Seddo",
    "position": "gopher",
  },
]
`

exports[`test/database/readme.js TAP retrieval: complex projection > matches result 1`] = `
Array [
  Object {
    "director": Object {
      "id": 1,
      "name": "Adip Singh",
      "tenure": 6544,
    },
    "founded": {time},
    "id": 1,
    "name": "carnegie 1",
    "patrons": Array [
      Object {
        "id": 1,
        "library_id": 1,
        "name": "Jesus Modtempor",
      },
    ],
    "postcode": "12345",
  },
]
`

exports[`test/database/readme.js TAP retrieval: count > matches result 1`] = `
Array [
  Object {
    "collection_size": "1",
    "id": 2,
  },
  Object {
    "collection_size": "12",
    "id": 1,
  },
]
`

exports[`test/database/readme.js TAP retrieval: expr > matches result 1`] = `
Array [
  Object {
    "id": 1,
    "tenure": "18",
  },
  Object {
    "id": 2,
    "tenure": "12",
  },
]
`

exports[`test/database/readme.js TAP retrieval: fully specified join > matches result 1`] = `
Array [
  Object {
    "founded": {time},
    "holdings": Array [
      Object {
        "book_id": 1,
        "books": Array [
          Object {
            "author_id": 3,
            "id": 1,
            "print_date": {time},
            "publisher": "Aleatory Domicile",
            "title": "The Placeholder",
            "v": Object {
              "author_id": null,
              "extra": null,
            },
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 2,
        "books": Array [
          Object {
            "author_id": 1,
            "id": 2,
            "print_date": null,
            "publisher": null,
            "title": "one",
            "v": Object {
              "author_id": 1,
              "extra": "text",
            },
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 3,
        "books": Array [
          Object {
            "author_id": 1,
            "id": 3,
            "print_date": null,
            "publisher": null,
            "title": "two",
            "v": Object {
              "author_id": 1,
              "extra": "text",
            },
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 4,
        "books": Array [
          Object {
            "author_id": 1,
            "id": 4,
            "print_date": null,
            "publisher": null,
            "title": "three",
            "v": Object {
              "author_id": 1,
              "extra": "text",
            },
          },
        ],
        "library_id": 1,
      },
      Object {
        "book_id": 5,
        "books": Array [],
        "library_id": 1,
      },
      Object {
        "book_id": 6,
        "books": Array [],
        "library_id": 1,
      },
      Object {
        "book_id": 7,
        "books": Array [],
        "library_id": 1,
      },
      Object {
        "book_id": 8,
        "books": Array [],
        "library_id": 1,
      },
      Object {
        "book_id": 9,
        "books": Array [],
        "library_id": 1,
      },
      Object {
        "book_id": 10,
        "books": Array [],
        "library_id": 1,
      },
      Object {
        "book_id": 11,
        "books": Array [],
        "library_id": 1,
      },
      Object {
        "book_id": 12,
        "books": Array [],
        "library_id": 1,
      },
    ],
    "id": 1,
    "name": "carnegie 1",
    "postcode": "12345",
  },
  Object {
    "founded": {time},
    "holdings": Array [
      Object {
        "book_id": 1,
        "books": Array [
          Object {
            "author_id": 3,
            "id": 1,
            "print_date": {time},
            "publisher": "Aleatory Domicile",
            "title": "The Placeholder",
            "v": Object {
              "author_id": null,
              "extra": null,
            },
          },
        ],
        "library_id": 2,
      },
    ],
    "id": 2,
    "name": "the bookplace",
    "postcode": "77788",
  },
]
`

exports[`test/database/readme.js TAP retrieval: ordering > matches result 1`] = `
Array [
  Object {
    "founded": {time},
    "id": 2,
    "name": "the bookplace",
    "postcode": "77788",
  },
  Object {
    "founded": {time},
    "id": 1,
    "name": "carnegie 1",
    "postcode": "12345",
  },
]
`

exports[`test/database/readme.js TAP retrieval: project exprs in $fields > matches result 1`] = `
Array [
  Object {
    "id": 1,
    "name": "Adip Singh",
    "tenure": 6544,
  },
]
`

exports[`test/database/readme.js TAP retrieval: project join into simple array > matches result 1`] = `
Array [
  Object {
    "hired_on": {time},
    "id": 1,
    "library_id": 1,
    "name": "Adip Singh",
    "position": "director",
    "postcode": "12345",
  },
  Object {
    "hired_on": {time},
    "id": 2,
    "library_id": 1,
    "name": "Eli T Seddo",
    "position": "gopher",
    "postcode": "12345",
  },
]
`

exports[`test/database/readme.js TAP retrieval: project simple array > matches result 1`] = `
Array [
  Object {
    "id": 1,
    "name": "carnegie 1",
  },
  Object {
    "id": 2,
    "name": "the bookplace",
  },
]
`
