/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/database/select.js TAP all select > join_child results match 1`] = `
Array [
  Object {
    "id": 2,
    "join_parent": Array [
      Object {
        "id": 2,
        "txt": "two",
      },
    ],
    "parent_id": 2,
    "txt": "two-one",
  },
]
`

exports[`test/database/select.js TAP all select > join_parent results match 1`] = `
Array [
  Object {
    "id": 1,
    "txt": "one",
  },
]
`

exports[`test/database/select.js TAP all select > regular_table results match 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/database/select.js TAP basic select > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table"
`

exports[`test/database/select.js TAP basic select > retrieves all rows 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/database/select.js TAP projects the first row of a complex projection with target.one > retrieves all rows 1`] = `
Object {
  "id": 1,
  "join_child": Array [
    Object {
      "id": 1,
      "parent_id": 1,
      "txt": "one-one",
    },
  ],
  "txt": "one",
}
`

exports[`test/database/select.js TAP projects the first row of a simple projection with target.one > retrieves all rows 1`] = `
Object {
  "id": 1,
  "parent_id": 1,
  "txt": "one",
}
`
