/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/database/target/list.js TAP returns a single column as an array for db.$target.list > matches query text 1`] = `
select "regular_table"."txt" as "txt" from "regular_table"
`

exports[`test/database/target/list.js TAP returns the first column no matter what else is added > matches query text 1`] = `
select "regular_table"."txt" as "txt", "regular_table"."id" as "id" from "regular_table"
`
