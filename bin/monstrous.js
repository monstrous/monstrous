#!/usr/bin/env node

/* eslint-disable no-console */

'use strict';

import { program } from 'commander';
import prompts from 'prompts';
import repl from 'repl';
import monstrous from '../index.js';
import pkg from '../package.json' assert { type: 'json' };

program
  .version(pkg.version)
  .option('-h, --host [host]', 'Connect to this server', 'localhost')
  .option('-p, --port [port]', 'On this port', 5432)
  .option('-U, --user [user]', 'As this user')
  .option('-W, --password', 'Prompt for a password')
  .option('-s, --scripts [dir]', 'Use this scripts directory', 'db')
  .argument('<database>', 'Open this database')
  .parse(process.argv);

const opts = program.opts();

if (opts.password) {
  opts.password = (await prompts({
    type: 'password',
    name: 'password',
    message: 'Password:'
  })).password;
}

opts.database = program.args[0];

if (!opts.database) {
  program.help();
}

monstrous(opts).then(db => {
  console.log('monstrous loaded and listening');

  const r = repl.start({
    prompt: 'db > ',
    eval: (cmd, ctx, f, callback) => {
      const result = eval(cmd);

      if (result && result.then) {
        return result.then(val => callback(null, val)).catch(err => callback(err));
      }

      return callback(null, result);
    }
  });

  r.context.db = db;
  r.on('exit', () => {
    process.exit(0);
  });
}).catch(err => {
  console.log(`Failed loading monstrous: ${err}`);
  process.exit(1);
});
