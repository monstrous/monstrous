# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.5.0](https://gitlab.com/monstrous/monstrous/compare/v0.4.0...v0.5.0) (2024-10-15)


### Features

* handle json and array function args ([18705c4](https://gitlab.com/monstrous/monstrous/commit/18705c4505cfffd0342d1558ae6a917d01bd8841))
* list target retrieves a column as a simple array ([11ddf50](https://gitlab.com/monstrous/monstrous/commit/11ddf50d7aded6760fe87a80b6a7594e4c2a0c0e))
* project db.count() with an optional expr ([ee7dffc](https://gitlab.com/monstrous/monstrous/commit/ee7dffc8994b2954c072d1ca94042836b67f0d0f))
* use exprs as db function arguments ([1d3cd90](https://gitlab.com/monstrous/monstrous/commit/1d3cd90e95d181aaf9fad225b785c57270ab5c78))


### Bug Fixes

* prevent gaps in placeholder indexing when passing exprs to functions ([3fcf8b6](https://gitlab.com/monstrous/monstrous/commit/3fcf8b6a1a6fd46571600479cc9971133b57d6c0))

## [0.4.0](https://gitlab.com/monstrous/monstrous/compare/v0.3.1...v0.4.0) (2023-08-06)


### ⚠ BREAKING CHANGES

* projection $columns has been renamed

### Features

* project literals ([e1d1edb](https://gitlab.com/monstrous/monstrous/commit/e1d1edb9ff1487d36d42f019a81779bbf6d75467))
* project nested relations as objects or arrays ([81de0ff](https://gitlab.com/monstrous/monstrous/commit/81de0ff91021613d82cc18f454cabad8d17096a0))


### Bug Fixes

* implement not-in ([6d971bb](https://gitlab.com/monstrous/monstrous/commit/6d971bb8ba92a23c818d1b3ca9214355a81d7c00))


* $columns -> $fields ([d9209a6](https://gitlab.com/monstrous/monstrous/commit/d9209a6f7bbe5719bac32d93d2d4224791485131))

## [0.3.1](https://gitlab.com/monstrous/monstrous/compare/v0.3.0...v0.3.1) (2023-07-26)


### Bug Fixes

* compile order() exprs ([0d1be96](https://gitlab.com/monstrous/monstrous/commit/0d1be9605bb50947434544a1a296c664fc2ea423))

## [0.3.0](https://gitlab.com/monstrous/monstrous/compare/v0.2.0...v0.3.0) (2023-07-16)


### Features

* db.query() raw sql ([0a8a991](https://gitlab.com/monstrous/monstrous/commit/0a8a991ca1dd9ede653c0a2ed90329f1cc46236a))
* inline ordinal arguments for exprs, fluent method for options ([3075db4](https://gitlab.com/monstrous/monstrous/commit/3075db4e205f4fde25e6a26b137d4d15b225ac78))

## [0.2.0](https://gitlab.com/monstrous/monstrous/compare/v0.1.6...v0.2.0) (2023-07-15)


### Features

* consistent argument behavior for functions and queryfiles ([bd259a4](https://gitlab.com/monstrous/monstrous/commit/bd259a48bea623654e244c1b0e139ad4afbace1d))

## [0.1.6](https://gitlab.com/monstrous/monstrous/compare/v0.1.5...v0.1.6) (2023-07-14)


### Bug Fixes

* preserve fk relation name when aliasing ([f2bb3ac](https://gitlab.com/monstrous/monstrous/commit/f2bb3ac8cbf8aaa23ef0f4ff90d815cbdc7d8d34))

## [0.1.5](https://gitlab.com/monstrous/monstrous/compare/v0.1.4...v0.1.5) (2023-03-27)


### Bug Fixes

* only attempt to insert known columns ([5bd1609](https://gitlab.com/monstrous/monstrous/commit/5bd1609119af7f637a39c46b4a0806d3cdc95cdb))

## [0.1.4](https://gitlab.com/monstrous/monstrous/compare/v0.1.3...v0.1.4) (2023-03-26)


### Bug Fixes

* correct error message typo ([edcd31b](https://gitlab.com/monstrous/monstrous/commit/edcd31b5c2dfccbfdd320e807905e859b116fc45))
* only set values for known columns in update ([62cb47f](https://gitlab.com/monstrous/monstrous/commit/62cb47fea8fead7d8711eaa68c4f21ce647698ce))

## [0.1.3](https://gitlab.com/monstrous/monstrous/compare/v0.1.2...v0.1.3) (2023-03-06)


### Bug Fixes

* compile exprs in predicate-tree mutators ([58920bf](https://gitlab.com/monstrous/monstrous/commit/58920bfe45bc346f7443ae4b5f984d787e43f7ba))

## [0.1.2](https://gitlab.com/monstrous/monstrous/compare/v0.1.1...v0.1.2) (2023-02-19)


### Bug Fixes

* projection clones set #to_array ([4409912](https://gitlab.com/monstrous/monstrous/commit/4409912f37fcd557bb0268297a0ed6d7d94801d6))

## [0.1.1](https://gitlab.com/monstrous/monstrous/compare/v0.1.0...v0.1.1) (2023-02-17)


### Features

* composite types with Tuple ([cdc881f](https://gitlab.com/monstrous/monstrous/commit/cdc881fbf2f2ec8203b9c837057a5a5f3be27a50))
