import Database from './lib/database/index.js';

export default (connection, driver_config = {}) =>
  new Database(connection, driver_config).reload();
