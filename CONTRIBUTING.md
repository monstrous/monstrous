# contributing

Please try to include as much information as you can in issues, especially:

- Node version
- monstrous version
- error messages and stack traces
- sample code

Merge requests with code changes should have accompanying tests. Try to stick to monstrous' code style -- yes, that means snake_case variable names :)
