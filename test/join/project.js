import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('renames a relation', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha
    .join(beta)
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project({
      $key: alpha.$id,
      $fields: [...alpha],
      beta_but_better: [{
        $key: beta.$id,
        $fields: [...beta]
      }]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins from a view with an explicit fully specified projection', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta_view = tap.context.db.foreign_keys.beta_view;
  const statement = beta_view
    .join(alpha, {[alpha.$id]: beta_view.$alpha_id})
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 3
    }).project({
      $key: [beta_view.$id],
      alpha_id: beta_view.$alpha_id,
      id: beta_view.$id,
      j: beta_view.$j,
      val: beta_view.$val,
      alpha: [
        {
          $key: alpha.$id,
          id: alpha.$id,
          val: alpha.$val
        }
      ]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins from a view with an explicit projection using $fields', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta_view = tap.context.db.foreign_keys.beta_view;
  const statement = beta_view
    .join(alpha, {[alpha.$id]: beta_view.$alpha_id})
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 3
    }).project({
      $key: beta_view.$id,
      $fields: [...beta_view],
      alpha: [
        {
          $key: alpha.$id,
          $fields: [...alpha]
        }
      ]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('omits a child relation from the final result', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const alpha_zeta = tap.context.db.foreign_keys.alpha_zeta;
  const statement = alpha
    .join(alpha_zeta, tap.context.db.$join.left, {[alpha_zeta.$alpha_id]: alpha.$id})
    .filter({
      [alpha_zeta.$zeta_id]: [1, 2]
    })
    .project([...alpha]);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2); // articulation is skipped for simple projections so no dedupe
  t.matchSnapshot(rows, 'matches results');
});

tap.test('omits a junction relation from the final result', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const alpha_zeta = tap.context.db.foreign_keys.alpha_zeta;
  const zeta = tap.context.db.foreign_keys.zeta;
  const statement = alpha
    .join(alpha_zeta, tap.context.db.$join.left, {[alpha_zeta.$alpha_id]: alpha.$id})
    .join(zeta, tap.context.db.$join.left, {[zeta.$id]: alpha_zeta.$zeta_id})
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project({
      $key: alpha.$id,
      $fields: [...alpha],
      zeta: [{
        $key: zeta.$id,
        $fields: [...zeta]
      }]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('omits a parent relation from the final result', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const alpha_zeta = tap.context.db.foreign_keys.alpha_zeta;
  const zeta = tap.context.db.foreign_keys.zeta;
  const statement = alpha
    .join(alpha_zeta, tap.context.db.$join.left, {[alpha_zeta.$alpha_id]: alpha.$id})
    .join(zeta, tap.context.db.$join.left, {[zeta.$id]: alpha_zeta.$zeta_id})
    .filter({
      [alpha.$id]: [1, 2]
    })
    .project({
      $key: alpha_zeta.$alpha_id,
      $fields: [...alpha_zeta],
      zeta: [{
        $key: zeta.$id,
        $fields: [...zeta]
      }]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('uses primary_key alongside qualified columns', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const alpha_zeta = tap.context.db.foreign_keys.alpha_zeta;
  const zeta = tap.context.db.foreign_keys.zeta;
  const statement = alpha
    .join(alpha_zeta, tap.context.db.$join.left, {[alpha_zeta.$alpha_id]: alpha.$id})
    .join(zeta, tap.context.db.$join.left, {[zeta.$id]: alpha_zeta.$zeta_id})
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project({
      $key: alpha.primary_key,
      $fields: [...alpha],
      zeta: [{
        $key: zeta.primary_key,
        $fields: [...zeta]
      }]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('uses primary_key instead of qualified columns', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const alpha_zeta = tap.context.db.foreign_keys.alpha_zeta;
  const zeta = tap.context.db.foreign_keys.zeta;
  const statement = alpha
    .join(alpha_zeta, tap.context.db.$join.left, {[alpha_zeta.$alpha_id]: alpha.$id})
    .join(zeta, tap.context.db.$join.left, {[zeta.$id]: alpha_zeta.$zeta_id})
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project({
      $key: alpha.primary_key,
      zeta: [{
        $key: zeta.primary_key,
        $fields: [...zeta]
      }]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('errors if $key is not specified', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta_view;
  const statement = alpha
    .join(beta, tap.context.db.$join.left, {[beta.$alpha_id]: alpha.$id})
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project({
      $fields: [...alpha],
      beta: [{
        $fields: [...beta]
      }]
    });

  return t.rejects(
    () => tap.context.db.select(statement).catch(err => {
      t.equal(err.message, 'attempted to articulate a row with a null or undefined root key; ensure that all columns are aliased uniquely and/or a complex projection schema defines $key for each output object or array');

      throw err;
    })
  );
});

tap.test('adds exprs to the join projection object', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = tap.context.db.foreign_keys.alpha
    .join(beta)
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 3
    })
    .project({
      $key: alpha.$id,
      $fields: [
        alpha.$id,
        tap.context.db.expr(`${alpha.$id} + 1`).as('aplus'),
        tap.context.db.tuple(
          tap.context.db.expr(`${alpha.$id} + 1`),
          'some text'
        ).as('tuple')
      ],
      beta: [{
        $key: beta.$id,
        $fields: [
          ...beta,
          tap.context.db.expr(`${beta.$id} + 1`).as('bplus')
        ]
      }]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('adds exprs to the join projection array', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = tap.context.db.foreign_keys.alpha
    .join(beta)
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 3
    })
    .project([
      alpha.$id,
      tap.context.db.expr(`${alpha.$id} + 1`).as('aplus'),
      beta.$id,
      tap.context.db.expr(`${beta.$id} + 1`).as('bplus')
    ]);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('errors if exprs are not aliased', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;

  t.throws(() => {
    tap.context.db.foreign_keys.alpha
      .join(beta)
      .filter({
        [tap.context.db.foreign_keys.alpha.$id]: 3
      })
      .project({
        $key: alpha.$id,
        $fields: [
          alpha.$id,
          tap.context.db.expr(`${alpha.$id} + 1`)
        ]
      })
    },
    new Error('projected exprs must be aliased with as()')
  );
});

tap.test('projects a literal with an object', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = tap.context.db.foreign_keys.alpha
    .join(beta)
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 3
    })
    .project({
      $key: alpha.primary_key,
      val: alpha.$val,
      literal: tap.context.db.literal('stuff'),
      bool: tap.context.db.literal(true),
      num: tap.context.db.literal(123),
      float: tap.context.db.literal(456.789),
      beta: {
        $key: beta.primary_key,
        literal: tap.context.db.literal(`more stuff'n'things`)
      }
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('projects a tuple with an object', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = tap.context.db.foreign_keys.alpha
    .join(beta)
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 3
    })
    .project({
      $key: alpha.primary_key,
      val: alpha.$val,
      tuple: tap.context.db.tuple(1, 2, 3, 'hello'),
      beta: {
        $key: beta.primary_key,
        tuple: tap.context.db.tuple(4, 5, `more stuff'n'things`)
      }
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('projects a join using tables as shortcuts', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const alpha_zeta = tap.context.db.foreign_keys.alpha_zeta;
  const zeta = tap.context.db.foreign_keys.zeta;
  const statement = alpha
    .join(beta)
    .join(alpha_zeta, tap.context.db.$join.left, {[alpha_zeta.$alpha_id]: alpha.$id})
    .join(zeta, tap.context.db.$join.left, {[zeta.$id]: alpha_zeta.$zeta_id})
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project({
      $key: alpha.primary_key,
      $fields: [...alpha],
      beta,
      zeta: [zeta]
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});
