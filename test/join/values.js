import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'general'));
tap.teardown(teardown(tap));

tap.test('joins a values list', async t => {
  const values = tap.context.db.values('v', {
    a: 1,
    b: 2
  }, {
    a: 3,
    b: 4
  });

  const statement = tap.context.db.join_parent.join(values, {
    [values.$a]: tap.context.db.join_parent.$id
  });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('joins a values list, inline', async t => {
  const statement = tap.context.db.join_parent.join(
    tap.context.db.values('v', {
      a: 1,
      b: 2
    }, {
      a: 3,
      b: 4
    }),
    {
      'v.a': tap.context.db.join_parent.$id
    }
  );

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables');
});
