import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('joins to a joined relation instead of the origin', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = alpha
    .join(beta, {[beta.$alpha_id]: alpha.$id})
    .join(gamma, {[gamma.$beta_id]: beta.$id})
    .filter({
      [`${tap.context.db.foreign_keys.alpha.$id} >`]: 1
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins involving a joined relation as well as the origin', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = alpha
    .join(beta, {[beta.$alpha_id]: alpha.$id}, tap.context.db.$join.left)
    .join(gamma, {
      [gamma.$alpha_id_one]: alpha.$id,
      [`${gamma.$beta_id} <>`]: beta.$id
    }, tap.context.db.$join.left)
    .filter({
      [`${tap.context.db.foreign_keys.alpha.$id} >`]: 1
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins to a joined relation instead of the origin, through an implicit foreign key relationship', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const delta = tap.context.db.foreign_keys_too.delta;
  const statement = alpha
    .join(beta)
    .join(delta) // has beta_id
    .filter({
      [`${tap.context.db.foreign_keys.alpha.$id} >`]: 1
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('cannot join a self-joining table to itself implicitly', async t => {
  const selfjoin = tap.context.db.foreign_keys.selfjoin;
  const sj = selfjoin.as('sj');
  t.throws(
    () => selfjoin.join(sj),
    new Error('multiple possible foreign keys for sj, specify an explicit join condition')
  );
});

tap.test('joins to a self-joining table', async t => {
  const selfjoin = tap.context.db.foreign_keys.selfjoin;
  const sj = selfjoin.as('sj');
  const statement = selfjoin
    .join(sj, {[sj.$parent_id]: selfjoin.$id});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins to a self-joining table, from another', async t => {
  const beta = tap.context.db.foreign_keys.beta;
  const selfjoin = tap.context.db.foreign_keys.selfjoin;
  const sj1 = selfjoin.as('sj1');
  const sj2 = selfjoin.as('sj2');
  const statement = tap.context.db.foreign_keys.beta
    .join(sj1, {[sj1.$beta_id]: beta.$id})
    .join(sj2, {[sj2.$parent_id]: sj1.$id});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins multiple tables at multiple levels', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const delta = tap.context.db.foreign_keys_too.delta;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = alpha
    .join(beta, {[beta.$alpha_id]: alpha.$id})
    .join(gamma, {[gamma.$beta_id]: beta.$id})
    .join(delta, {[delta.$beta_id]: beta.$id})
    .join(epsilon, {[epsilon.$alpha_id]: alpha.$id})
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 1
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});
