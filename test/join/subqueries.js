import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'general'));
tap.teardown(teardown(tap));

tap.test('joins a subquery', async t => {
  const subquery = tap.context.db.join_child.filter({id: 1});
  const statement = tap.context.db.join_parent
    .join(
      subquery,
      {[subquery.$parent_id]: tap.context.db.join_parent.$id}
    );
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('joins a subquery, aliased', async t => {
  const subquery = tap.context.db.join_child.filter({id: 1}).as('jc');
  const statement = tap.context.db.join_parent
    .join(
      subquery,
      {[subquery.$parent_id]: tap.context.db.join_parent.$id}
    );
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('joins a subquery, inline, aliased, qualified', async t => {
  const statement = tap.context.db.join_parent
    .join(
      tap.context.db.join_child.filter({id: 1}).as('jc'),
      {'jc.parent_id': tap.context.db.join_parent.$id}
    );
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('joins a subquery, inline, aliased, unqualified', async t => {
  const statement = tap.context.db.join_parent
    .join(
      tap.context.db.join_child.filter({id: 1}).as('jc'),
      {parent_id: tap.context.db.join_parent.$id}
    );
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('joins a subquery, inline, unaliased, unqualified', async t => {
  const statement = tap.context.db.join_parent
    .join(
      tap.context.db.join_child.filter({id: 1}),
      {parent_id: tap.context.db.join_parent.$id}
    );
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('joins a subquery for insert but does nothing with it', async t => {
  const subquery = tap.context.db.join_child.filter({id: 1});
  const statement = tap.context.db.join_parent
    .join(
      subquery,
      {[subquery.$parent_id]: tap.context.db.join_parent.$id}
    );
  const text = await tap.context.db.insert(statement, {txt: 'what now?'}, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, {txt: 'what now?'});

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');

  const child_rows = await tap.context.db.select(
    tap.context.db.join_child.project({count: tap.context.db.expr('count(*)')}),
    tap.context.db.$target.unit
  );

  t.equal(Number(child_rows), 3);
});

tap.test('errors joining a subquery without an explicit `on`', async t => {
  const subquery = tap.context.db.join_child.filter({id: 1});

  t.throws(
    () => tap.context.db.join_parent.join(subquery),
    new Error('bad join definition: non-tables require explicit `on` criteria')
  );
});

tap.test('errors joining a subquery before a relation of the same name', async t => {
  const subquery = tap.context.db.join_child.filter({id: 1}); // alias is the rel name
  const statement = tap.context.db.join_parent
    .join(
      subquery,
      {[subquery.$parent_id]: tap.context.db.join_parent.$id}
    )
    .join(tap.context.db.join_child);

  return t.rejects(
    () => tap.context.db.select(statement).catch(err => {
      t.equal(err.code, '42712', 'errcode correct');
      t.equal(err.message, 'table name "join_child" specified more than once', 'error message correct');

      throw err;
    })
  );
});

tap.test('errors joining a subquery after a relation of the same name', async t => {
  const subquery = tap.context.db.join_child.filter({id: 1}); // alias is the rel name
  const statement = tap.context.db.join_parent
    .join(tap.context.db.join_child)
    .join(
      subquery,
      {[subquery.$parent_id]: tap.context.db.join_parent.$id}
    );

  return t.rejects(
    () => tap.context.db.select(statement).catch(err => {
      t.equal(err.code, '42712', 'errcode correct');
      t.equal(err.message, 'table name "join_child" specified more than once', 'error message correct');

      throw err;
    })
  );
});
