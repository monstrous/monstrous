import {v4} from 'uuid';
import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('inserts the origin of a table-only join', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta);

  const value = {
    val: 'new and improved',
    beta: [{
      val: 'in beta'
    }, {
      val: 'in beta too'
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 5);
  t.matchSnapshot(rows, 'alpha matches results');

  const beta_rows = await tap.context.db.select(beta.filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta matches results');
});

tap.test('inserts the origin of a table-only join over uuids, with more uuids', async t => {
  const parent = tap.context.db.foreign_keys.uuid_parent;
  const child = tap.context.db.foreign_keys.uuid_child.as('child');
  const statement = parent.join(child);

  const value = {
    val: 'new and improved',
    uu: v4(),
    child: [{
      val: 'in child',
      uu: v4()
    }, {
      val: 'in child too',
      uu: v4()
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'parent matches results');

  const child_rows = await tap.context.db.select(child.filter({[child.$parent_id]: rows[0].id}));

  t.matchSnapshot(child_rows, 'child matches results');
});

tap.test('inserts the origin of a join with an expr', async t => {
  const parent = tap.context.db.foreign_keys.uuid_parent;
  const child = tap.context.db.foreign_keys.uuid_child.as('child');
  const statement = parent.join(child);

  const value = {
    val: tap.context.db.expr(`now()::text || ' with an ' || $1`, 'expr!'),
    child: [{
      val: tap.context.db.expr(`now()::text || ' with another ' || $1`, 'expr!'),
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'parent matches results');

  const child_rows = await tap.context.db.select(child.filter({[child.$parent_id]: rows[0].id}));

  t.matchSnapshot(child_rows, 'child matches results');
});

tap.test('uses fully-qualified names', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta);

  const value = {
    val: 'newer and improveder',
    [beta.path]: [{
      val: 'in beta'
    }, {
      val: 'in beta too'
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 6);
  t.matchSnapshot(rows, 'alpha matches results');

  const beta_rows = await tap.context.db.select(beta.filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta matches results');
});

tap.test('inserts multiple dependents', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = alpha.join(beta).join(epsilon);

  const value = {
    val: 'newest and improvedest',
    beta: [{
      val: 'in beta'
    }, {
      val: 'in beta too'
    }],
    epsilon: [{
      val: 'in epsilon'
    }, {
      val: 'in epsilon again'
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 7);
  t.matchSnapshot(rows, 'matches results');

  const beta_rows = await tap.context.db.select(beta.filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta matches results');

  const epsilon_rows = await tap.context.db.select(epsilon.filter({alpha_id: rows[0].id}));

  t.matchSnapshot(epsilon_rows, 'epsilon matches results');
});

// TODO use gamma to test alpha & beta foreign keys simultaneously -- alpha ids
// will have to be identical or one null, should depend on join
tap.test('inserts longer recursive chains', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const delta = tap.context.db.foreign_keys_too.delta;
  const statement = alpha.join(beta).join(delta);

  const value = {
    val: 'novel and refined',
    beta: [{
      val: 'in beta',
      delta: [{
        val: 'in delta'
      }, {
        val: 'in delta again'
      }]
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 8);
  t.matchSnapshot(rows, 'matches results');

  const beta_rows = await tap.context.db.select(beta.join(delta).filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta and delta match results');
});

tap.test('allows objects', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const delta = tap.context.db.foreign_keys_too.delta;
  const statement = alpha.join(beta).join(delta);

  const value = {
    val: 'novel and refined',
    beta: {
      val: 'in beta',
      delta: {
        val: 'in delta'
      }
    }
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 9);
  t.matchSnapshot(rows, 'matches results');

  const beta_rows = await tap.context.db.select(beta.join(delta).filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta and delta match results');
});

tap.test('expresses recursive chains with flattened values', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const delta = tap.context.db.foreign_keys_too.delta;
  const statement = alpha.join(beta).join(delta);

  const value = {
    val: 'current and enriched',
    beta: [{
      val: 'in beta',
    }],
    // delta has a foreign key to beta, so the previous test nests it under
    // beta's value, but because we track the foreign keys through the join we
    // can also process this form
    delta: [{
      val: 'in delta'
    }, {
      val: 'in delta again'
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 10);
  t.matchSnapshot(rows, 'matches results');

  const beta_rows = await tap.context.db.select(beta.join(delta).filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta and delta match results');
});

tap.test('works with aliased non-root relations', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const b = tap.context.db.foreign_keys.beta.as('b');
  const statement = alpha.join(b);

  const value = {
    val: 'new and improved',
    b: [{
      val: 'in b'
    }, {
      val: 'in b too'
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 11);
  t.matchSnapshot(rows, 'alpha matches results');

  const b_rows = await tap.context.db.select(tap.context.db.foreign_keys.beta.filter({alpha_id: rows[0].id}));

  t.matchSnapshot(b_rows, 'beta as b matches results');
});

tap.test('ignores superfluous fields in root relation', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta);

  const value = {
    val: 'new and improved',
    nonexistent: 'ignore me',
    beta: [{
      val: 'in beta'
    }, {
      val: 'in beta too'
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'alpha matches results');

  const beta_rows = await tap.context.db.select(beta.filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta matches results');
});

tap.test('ignores superfluous fields deeper in the join', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta);

  const value = {
    val: 'new and improved',
    beta: [{
      val: 'in beta',
      also_nonexistent: 'also ignored'
    }, {
      val: 'in beta too',
      also_nonexistent: 'also ignored'
    }]
  };

  const text = await tap.context.db.insert(statement, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(statement, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'alpha matches results');

  const beta_rows = await tap.context.db.select(beta.filter({alpha_id: rows[0].id}));

  t.matchSnapshot(beta_rows, 'beta matches results');
});

tap.test('errors on a top-level array', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta);

  const values = [{
    val: 'new and improved',
    beta: [{
      val: 'in beta'
    }, {
      val: 'in beta too'
    }]
  }, {
    val: 'newer and improveder',
    beta: [{
      val: 'in beta three'
    }, {
      val: 'in beta four'
    }]
  }];

  t.throws(
    () => tap.context.db.insert(statement, ...values),
    new Error('multi-table inserts may only add multiple records to leaf tables')
  );
});

tap.test('errors on intermediary arrays', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const delta = tap.context.db.foreign_keys_too.delta;
  const statement = alpha.join(beta).join(delta);

  // upon inserting both betas in a single cte it's impossible to track which
  // corresponds to which deltas; this could be supported through multiple beta
  // ctes, but that adds a lot of complexity
  const value = {
    val: 'new and improved',
    beta: [{
      val: 'in beta',
      delta: [{
        val: 'in delta'
      }, {
        val: 'in delta again'
      }]
    }, {
      val: 'in beta too',
      delta: [{
        val: 'in delta a third time'
      }, {
        val: 'in delta a fourth'
      }]
    }]
  };

  t.throws(
    () => tap.context.db.insert(statement, value),
    new Error('multi-table inserts may only add multiple records to leaf tables')
  );
});
