import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('counts', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha
    .join(beta)
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project([tap.context.db.count()])

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('counts an expr', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha
    .join(beta)
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project([tap.context.db.count(tap.context.db.expr('distinct beta.id'))])

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('aliases count', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha
    .join(beta)
    .filter({
      [alpha.$id]: [1, 3]
    })
    .project([tap.context.db.count(tap.context.db.expr('distinct beta.id').as('total'))])

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});
