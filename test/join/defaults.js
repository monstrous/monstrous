import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('autogenerates keys when one possible join fk matches', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter({[alpha.$id]: 3});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('autogenerates keys when one possible origin fk matches', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = beta.join(alpha).filter({[alpha.$id]: 3});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('autogenerates keys deeper in the join tree', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const delta = tap.context.db.foreign_keys_too.delta;
  const statement = alpha.join(beta).join(delta).filter({[`${alpha.$id} >`]: 1});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('errors if keys are not specified and there are no possible fks', async t => {
  const beta = tap.context.db.foreign_keys.beta;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;

  t.throws(
    () => beta.join(epsilon),
    new Error('no join condition and no available foreign key relationship for epsilon')
  );
});

tap.test('errors if keys are not specified and multiple possible fks match', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const gamma = tap.context.db.foreign_keys.gamma;

  t.throws(
    () => gamma.join(alpha),
    new Error('multiple possible foreign keys for alpha, specify an explicit join condition')
  );
});

tap.test('defaults to inner joins', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter({[`${alpha.$id} >`]: 1});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches results');
});

tap.skip('does a basic inner join with the bare minimum object', async t => {
  return db.alpha.join({
    beta: true
  }).find({
    'alpha.id >': 1
  }).then(result => {
    assert.deepEqual(result, [{
      id: 2, val: 'two', beta: [{id: 2, alpha_id: 2, j: null, val: 'alpha two'}]
    }, {
      id: 3, val: 'three', beta: [{
        id: 3, alpha_id: 3, j: null, val: 'alpha three'
      }, {
        id: 4, alpha_id: 3, j: null, val: 'alpha three again'
      }]
    }]);
  });
});

tap.skip('does a basic inner join with just a string', async t => {
  return db.alpha.join('beta').find({
    'alpha.id >': 1
  }).then(result => {
    assert.deepEqual(result, [{
      id: 2, val: 'two', beta: [{id: 2, alpha_id: 2, j: null, val: 'alpha two'}]
    }, {
      id: 3, val: 'three', beta: [{
        id: 3, alpha_id: 3, j: null, val: 'alpha three'
      }, {
        id: 4, alpha_id: 3, j: null, val: 'alpha three again'
      }]
    }]);
  });
});

tap.skip('shortcuts criteria keys without path info to the primary table', async t => {
  return db.alpha.join({
    beta: {
      type: 'INNER',
      on: {alpha_id: 'id'}
    }
  }).find({
    id: 3
  }).then(result => {
    assert.deepEqual(result, [{
      id: 3, val: 'three',
      beta: [{
        id: 3, alpha_id: 3, j: null, val: 'alpha three'
      }, {
        id: 4, alpha_id: 3, j: null, val: 'alpha three again'
      }]
    }]);
  });
});

tap.skip('shortcuts ordering keys without path info to the primary table', async t => {
  return db.alpha.join({
    beta: {
      type: 'INNER',
      on: {alpha_id: 'id'}
    }
  }).find({
    'alpha.id': 3
  }, {
    order: [{field: 'val', direction: 'desc'}],
    build: true
  }).then(result => {
    assert.equal(result.sql, [
      'SELECT "alpha"."id" AS "alpha__id",',
      '"alpha"."val" AS "alpha__val",',
      '"beta"."alpha_id" AS "beta__alpha_id",',
      '"beta"."id" AS "beta__id","beta"."j" AS "beta__j","beta"."val" AS "beta__val" ',
      'FROM "alpha" ',
      'INNER JOIN "beta" ON "beta"."alpha_id" = "alpha"."id" ',
      'WHERE "alpha"."id" = $1 ',
      'ORDER BY "alpha"."val" DESC'
    ].join(''));
    assert.deepEqual(result.params, [3]);
  });
});
