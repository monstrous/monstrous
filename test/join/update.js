import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('updates the origin of a join based on origin criteria', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter({[alpha.$id]: 1});

  const changes = {val: 'updated'};

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 1);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('updates the origin of a join with an expr', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter({[alpha.$id]: 1});

  const changes = {val: tap.context.db.expr(`${alpha.$val} || ' with an ' || $1`, 'expr!')};

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 1);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('updates the origin of a join based on dependent criteria', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = alpha
    .join(gamma, {[alpha.$id]: gamma.$alpha_id_two})
    .filter({[gamma.$beta_id]: 2});

  const changes = {val: 'also updated'};

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('updates the origin of a join with constant criteria', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta, {[beta.$id]: 1}).filter({[alpha.$id]: 2});

  const changes = {val: 'also also updated'};

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 2);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('updates the origin of a join to a dependent value with constant criteria', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta, {[beta.$id]: 1}).filter({[alpha.$id]: 2});

  const changes = {val: beta.$val};

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 2);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('updates the origin of a deeply nested join', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = alpha
    .join(beta)
    .join(gamma, {[gamma.$beta_id]: beta.$id})
    .filter({[gamma.$id]: 3});

  const changes = {val: 'also also also updated'};

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 2);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('updates all rows in the origin', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta);

  const changes = {val: 'updated'};

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('ignores non-column keys', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter({[alpha.$id]: 1});

  const changes = {
    val: 'updated',
    val2: 'this is a beta field',
    val123: 'this is a nonexistent field'
  };

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 1);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('uses qualified keys', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter({[alpha.$id]: 1});

  const changes = {
    [alpha.$val]: 'updated',
    [beta.$val2]: 'this is a beta field'
  };

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 1);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('errors if multiple relations joined to the origin', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = alpha
    .join(beta)
    .join(gamma, {[gamma.$alpha_id_one]: alpha.$id})
    .filter({[gamma.$id]: 4});

  const changes = {val: 'nope'};

  return t.rejects(
    () => tap.context.db.update(statement, changes).catch(err => {
      t.equal(err.code, '42P01', 'errcode correct');
      t.equal(err.message, 'invalid reference to FROM-clause entry for table "alpha"', 'error message correct');

      throw err;
    })
  );
});
