import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('delete-joins', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = gamma
    .join(beta)
    .join(alpha, {[alpha.$id]: beta.$alpha_id})
    .filter({[alpha.$id]: 3});

  const text = await tap.context.db.delete(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.delete(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('delete-joins all rows in the origin', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = gamma
    .join(beta)
    .join(alpha, {[alpha.$id]: beta.$alpha_id});

  const text = await tap.context.db.delete(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.delete(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'alpha matches results');
});

tap.test('runtime errors on out-of-bounds references to the FROM table after USING', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = beta
    .join(alpha)
    .join(gamma, {[gamma.$beta_id]: beta.$id})
    .filter({[gamma.$id]: 1});

  return t.rejects(
    () => tap.context.db.delete(statement).catch(err => {
      t.equal(err.code, '42P01', 'errcode correct');
      t.equal(err.message, 'invalid reference to FROM-clause entry for table "beta"', 'error message correct');

      throw err;
    })
  );
});
