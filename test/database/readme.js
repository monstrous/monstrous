import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'readme'));
tap.teardown(teardown(tap));

tap.test('persistence: insert', async t => {
  const db = tap.context.db;

  const result = await db.insert(db.authors, {name: 'Lauren Ipsum'}, {name: 'Daler S. Ahmet'});

  t.matchSnapshot(result, 'matches result');
});

tap.test('persistence: insert into a join', async t => {
  const db = tap.context.db;

  const result = await db.insert(
    db.authors
      .join(db.books)
      .join(db.holdings),
    {
      name: 'Consuela Ctetur',
      books: [{
        title: 'The Placeholder',
        publisher: 'Aleatory Domicile',
        print_date: new Date(2001, 0),
        // only "leaf" nodes can have multiple values!
        holdings: [{
          library_id: 1
        }, {
          library_id: 2
        }]
      }]
    },
    db.$target.one
  );

  t.matchSnapshot(result, 'matches result');

  const consuelas = await db.select(
    db.books.join(db.holdings).filter({[db.books.$author_id]: result.id})
  );

  t.matchSnapshot(consuelas, 'matches inserted');
});

tap.test('persistence: insert subquery, also tasks', async t => {
  const db = tap.context.db;

  let result;

  await db.transaction(async tx => {
    await tx.insert(db.books,
      {author_id: 1, title: 'one'},
      {author_id: 1, title: 'two'},
      {author_id: 1, title: 'three'},
      {author_id: 1, title: 'four'},
      {author_id: 1, title: 'five'},
      {author_id: 1, title: 'six'},
      {author_id: 1, title: 'seven'},
      {author_id: 1, title: 'eight'},
      {author_id: 1, title: 'nine'},
      {author_id: 1, title: 'ten'},
      {author_id: 1, title: 'eleven'},
    );

    result = await tx.insert(db.holdings,
      db.books
        .filter({author_id: 1})
        .project({
          book_id: db.books.$id,
          library_id: 1
        })
    );
  });

  t.matchSnapshot(result, 'matches result');
});

tap.test('persistence: update with exprs', async t => {
  const db = tap.context.db;

  const check_date = new Date(2000, 9);
  const result = await db.update(
    db.authors
      .join(db.books)
      .filter({publisher: 'Aleatory Domicile'}),
    {
      is_in_print: db.expr('case when print_date > $1 then true else false end', check_date)
    });

  t.matchSnapshot(result, 'matches result');
});

tap.test('persistence: save', async t => {
  const db = tap.context.db;

  const result = await db.save(db.authors, {
    id: 1,
    birth: new Date(1920, 1, 1),
    death: new Date(1999, 2, 1)
  });

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: fully specified join', async t => {
  const db = tap.context.db;

  const values = db.values('v', {author_id: 1, extra: 'text'});
  const result = await db.select(
    db.libraries
      .join(db.holdings, db.$join.left, {[db.holdings.$library_id]: db.libraries.$id})
      .join(
        // left-joining the filtered subquery means some holdings will be empty!
        db.books.filter({[`${db.books.$id} <`]: 5}),
        db.$join.left,
        {[db.holdings.$book_id]: db.books.$id}
      )
      .join(
        values,
        db.$join.left,
        {[values.$author_id]: db.books.$author_id}
      )
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: project simple array', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.libraries.project([db.libraries.$id, db.libraries.$name])
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: project join into simple array', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.employees
      .join(db.libraries)
      .project([...db.employees, db.libraries.$postcode])
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: complex projection', async t => {
  const db = tap.context.db;

  const directors = db.employees
    .filter({position: 'director'})
    .as('director');

  const result = await db.select(
    db.libraries
      .join(directors, {[directors.$library_id]: db.libraries.$id})
      .join(db.patrons)
      .project({
        $key: db.libraries.primary_key,
        $fields: [...db.libraries],
        director: { // override the subquery alias to singular
          $key: directors.$id,
          name: directors.$name,
          // replace now() since it would change after snapshotting
          tenure: db.expr(`'2023-01-01'::date - ${directors.$hired_on}`)
        },
        patrons: [db.patrons] // project entire patron records into an array
      })
    );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: project exprs in $fields', async t => {
  const db = tap.context.db;

  const directors = db.employees
    .filter({position: 'director'})
    .as('director');

  const result = await db.select(
    db.libraries
      .join(directors, {[directors.$library_id]: db.libraries.$id})
      .project({
        $key: directors.$id,
        $fields: [
          db.libraries.$name,
          directors.$name,
          // replace now() since it would change after snapshotting
          db.expr(`'2023-01-01'::date - ${directors.$hired_on}`).as('tenure')
        ]
      })
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: count', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.libraries
      .join(db.holdings)
      .project([
        db.libraries.$id,
        db.count(db.expr('holdings.*').as('collection_size'))
      ])
      .group(db.libraries.$id)
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: aliasing', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.employees.join(db.libraries.as('bookplaces'))
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: ordering', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.libraries.order(db.$order.desc(db.libraries.$founded), db.libraries.$name)
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('retrieval: expr', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.employees.project([
      db.employees.$id,
      db.expr(`extract(years from justify_interval($1::timestamptz - ${db.employees.$hired_on}))`, new Date(2023, 0)).as('tenure')
    ])
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('attachment', async t => {
  const db = tap.context.db;

  const ipsum = db.libraries
    .join(db.holdings)
    .join(db.books)
    .join(db.authors)
    .filter({[`${db.authors.$name} ilike`]: 'lauren%ipsum'});

  // don't do this on an application hot path!
  db.attach(ipsum, ['saved_queries', 'ipsum']);

  const result = await db.select(
    db.saved_queries.ipsum.filter({[db.libraries.$postcode]: '12345'})
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('fluency', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.libraries
      .join(db.holdings)
      .join(db.books)
      .filter({[db.libraries.$postcode]: '12345'})
      .join(db.authors)
      .filter({[`${db.authors.$name} ilike`]: 'lauren%ipsum'})
      .limit(10)
      .limit(5)
  );

  t.equal(result.length, 1);
  t.equal(result[0].holdings.length, 5);
  t.matchSnapshot(result, 'matches result');
});

tap.test('raw sql: ordinal', async t => {
  const db = tap.context.db;

  const result = await db.query(
    `select * from books where author_id = $1`,
    3
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('raw sql: named', async t => {
  const db = tap.context.db;

  const result = await db.query(
    `select * from books where author_id = $(author_id) and title ilike $(title)`,
    {
      author_id: 3,
      title: 'the placeholder'
    },
    db.$target.one
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('all together', async t => {
  const db = tap.context.db;

  const result = await db.select(
    db.libraries
      .join(db.holdings) // implicit join on foreign key holdings.library_id
      .join(db.books)    // implicit join on foreign key holdings.book_id
      .join(db.authors, db.$join.left, {[db.authors.$id]: db.books.$author_id})
      .filter({
        [db.libraries.$postcode]: '12345',
        [`${db.authors.$name} ilike`]: 'Lauren%Ipsum'
      })
      .project({
        $key: db.libraries.$id,
        $fields: [...db.libraries],
        authors: [{
          $key: db.authors.$id,
          $fields: [
            db.authors.$name,
            db.expr(
              `extract(year from age(coalesce(${db.authors.$death}, now()), ${db.authors.$birth}))`
            ).as('age')
          ],
          // notice `books` is a collection on authors, even though we join authors to books!
          books: [{
            $key: db.books.$id,
            $fields: [...db.books]
          }]
        }]
      })
  );

  t.matchSnapshot(result, 'matches result');
});

tap.test('persistence: delete', async t => {
  const db = tap.context.db;

  const result = await db.delete(
    db.holdings.join(db.books).filter({[db.books.$author_id]: 3})
  );

  t.matchSnapshot(result, 'matches result');
});
