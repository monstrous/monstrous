import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('attach a statement', async t => {
  const statement = tap.context.db.regular_table
    .filter(tap.context.db.expr('num <= 2.5'))
    .limit(1);

  tap.context.db.attach(statement, ['my_statement']);
  const text = await tap.context.db.select(tap.context.db.my_statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(tap.context.db.my_statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('attach a statement deep in the tree', async t => {
  const statement = tap.context.db.regular_table
    .filter(tap.context.db.expr('num <= 2.5'))
    .limit(1);

  tap.context.db.attach(statement, ['one', 'two', 'my_statement']);
  const text = await tap.context.db.select(tap.context.db.one.two.my_statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(tap.context.db.one.two.my_statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});
