import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'func'));
tap.teardown(teardown(tap));

tap.test('execute a database function', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.simple_func,
    1,
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('execute a multi-argument database function', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.two_arg_func,
    1,
    2,
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('pass an expr to a database function', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.two_arg_func,
    tap.context.db.expr('1 + $1', 2),
    1,
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('pass arrays to a database function', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.arr_func,
    [1, 2],
    [3.0, 4.1],
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('pass json to a database function', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.json_func,
    {key: 'value'},
    [1, 'str', {key: 'value'}],
    {key: 'value'},
    [1, 'str', {key: 'value'}],
    [2, 'str', {key: 'the array is a regular array!'}],
    [2, 'str', {key: 'the array is a regular array!'}],
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('pass uuids to a database function', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.uuid_func,
    '2aa86d83-3658-402c-a3a0-05b0df9d7a44',
    [
      'a7a8bc08-7969-4ccf-b310-acb8f992d7c7',
      'fafd0826-2025-47a1-9ff5-f31d46afc68b'
    ],
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('errors if too many args are supplied', async t => {
  t.throws(
    () => tap.context.db.execute(
      tap.context.db.simple_func,
      1,
      2,
      tap.context.db.$target.unit
    ),
    new Error(`too many arguments for function simple_func!`)
  );
});

tap.test('run a queryfile', async t => {
  const result = await tap.context.db.execute(tap.context.db.many);

  t.matchSnapshot(result, 'matches results');
});

tap.test('run a queryfile with ordinal parameters', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.ordinal,
    1,
    2,
    tap.context.db.$target.one
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('run a queryfile with named parameters', async t => {
  const result = await tap.context.db.execute(tap.context.db.named, {
    first: 1,
    second: 2
  });

  t.matchSnapshot(result, 'matches results');
});

tap.test('supply the `one` target to a queryfile', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.one,
    'text',
    tap.context.db.$target.one
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('supply the `unit` target to a queryfile', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.one,
    'text',
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});
