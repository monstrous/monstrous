import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('has table', async t => {
  t.ok(tap.context.db.regular_table);
});

tap.test('basic select', async t => {
  const statement = tap.context.db.regular_table;
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 5);
  t.matchSnapshot(rows, 'retrieves all rows');
});

tap.test('projects the first row of a simple projection with target.one', async t => {
  const statement = tap.context.db.join_parent
    .join(tap.context.db.join_child)
    .project([
      ...tap.context.db.join_parent,
      tap.context.db.join_child.$parent_id
    ]);
  const result = await tap.context.db.select(statement, tap.context.db.$target.one);

  t.type(result, 'object');
  t.matchSnapshot(result, 'retrieves all rows');
});

tap.test('projects the first row of a complex projection with target.one', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child);
  const result = await tap.context.db.select(statement, tap.context.db.$target.one);

  t.type(result, 'object');
  t.matchSnapshot(result, 'retrieves all rows');
});

tap.test('all select', async t => {
  const statement = tap.context.db.regular_table;
  const [rt, jp, jc] = await tap.context.db.all(
    tap.context.db.regular_table,
    tap.context.db.join_parent.filter(1),
    tap.context.db.join_child.join(tap.context.db.join_parent).filter(2)
  );

  t.matchSnapshot(rt, 'regular_table results match');
  t.matchSnapshot(jp, 'join_parent results match');
  t.matchSnapshot(jc, 'join_child results match');
});
