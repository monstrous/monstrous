import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('returns a single column as an array for db.$target.list', async t => {
  const regular_table = tap.context.db.regular_table;
  const statement = regular_table.project([regular_table.$txt]);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const value = await tap.context.db.select(statement, tap.context.db.$target.list);

  t.matchSnapshot(text, 'matches query text');
  t.same(value, ['one', 'two', 'three', 'four', 'five']);
});

tap.test('returns the first column no matter what else is added', async t => {
  const regular_table = tap.context.db.regular_table;
  const statement = regular_table.project([regular_table.$txt, regular_table.$id]);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const value = await tap.context.db.select(statement, tap.context.db.$target.list);

  t.matchSnapshot(text, 'matches query text');
  t.same(value, ['one', 'two', 'three', 'four', 'five']);
});
