import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('filter with operation criteria', async t => {
  const statement = tap.context.db.regular_table.filter({ 'bool is': true });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with implicit array criteria', async t => {
  const statement = tap.context.db.regular_table.filter({
    num: [1, 2, 3]
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with explicit array criteria', async t => {
  const statement = tap.context.db.regular_table.filter({
    'num =': [1, 2, 3]
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter not-in array criteria', async t => {
  const statement = tap.context.db.regular_table.filter({
    'num <>': [1, 2, 3]
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 4);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with compound criteria', async t => {
  const statement = tap.context.db.regular_table.filter({
    txt: 'three',
    'bool is': true
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.only('filter json with traversal', async t => {
  const table = tap.context.db.json_table;
  const statement = table.filter({
    [`(${table.$js} ->> 'alpha')::int`]: 1
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter json with subscripting', async t => {
  const table = tap.context.db.json_table;
  const statement = table.filter({
    [`${table.$js}['alpha']::int`]: 1
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter overrides op for nulls', async t => {
  const statement = tap.context.db.join_ettin.filter({ettin_id: null});
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('logically impossible key combinations can happen', async t => {
  const statement = tap.context.db.regular_table
    .filter({txt: 'three'})
    .filter({txt: 'two'});
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 0);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with expr', async t => {
  const statement = tap.context.db.regular_table.filter(tap.context.db.expr('num <= 2.5'));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with expr criteria value', async t => {
  const statement = tap.context.db.regular_table.filter({
    [`${tap.context.db.regular_table.$num} <=`]: tap.context.db.expr('5.0 / 2')
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with interpolated-column expr', async t => {
  const statement = tap.context.db.regular_table.filter(tap.context.db.expr(`${tap.context.db.regular_table.$num} <= 2.5`));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with int primary key', async t => {
  const statement = tap.context.db.regular_table.filter(1);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with string', async t => {
  const statement = tap.context.db.regular_table.filter('num <= 2.5');
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter column to column', async t => {
  const regular_table = tap.context.db.regular_table;
  const statement = regular_table.filter({
    [regular_table.$id]: regular_table.$num
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter column to column with operation', async t => {
  const regular_table = tap.context.db.regular_table;
  const statement = regular_table.filter({
    [`${regular_table.$id} >`]: regular_table.$num
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 0);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with parameterized expr', async t => {
  const statement = tap.context.db.regular_table.filter(tap.context.db.expr('num <= $1', 2.5));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with named-parameterized expr', async t => {
  const statement = tap.context.db.regular_table.filter(tap.context.db.expr('num <= $(val)', {
    val: 2.5
  }));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with combination of criteria and expr', async t => {
  const statement = tap.context.db.regular_table.filter({
    txt: 'one',
    'bool is': true
  },
    tap.context.db.expr('num <= 2.5')
  );
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with nested conjunction', async t => {
  const statement = tap.context.db.regular_table.filter({
    $and: [
      {txt: 'one'},
      {'bool is': true}
    ]
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter with nested disjunction', async t => {
  const statement = tap.context.db.regular_table.filter({
    $or: [
      {txt: 'one'},
      {'bool is': true}
    ]
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('filter in subquery', async t => {
  const statement = tap.context.db.regular_table.filter({
    txt: tap.context.db.join_parent
      .filter({[`${tap.context.db.join_parent.$id} >`]: 1})
      .project([tap.context.db.join_parent.$txt])
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('throws if given a non-criteria non-expr non-string object', async t => {
  t.throws(
    () => tap.context.db.regular_table.filter(new Date()),
    new Error('expected criteria object, expr, string, or boolean; got [object Date]')
  );
});
