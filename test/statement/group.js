import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('group by and aggregate', async t => {
  const table = tap.context.db.regular_table;
  const statement = table
    .group(table.$bool)
    .project({
      $fields: [table.$bool],
      count: tap.context.db.expr('count(*)')
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'matches query result');
});
