import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('offsets', async t => {
  const statement = tap.context.db.regular_table.offset(2);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'retrieves tailing rows');
});

tap.test('limits', async t => {
  const statement = tap.context.db.regular_table.limit(2);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'retrieves some rows');
});

tap.test('offsets and limits', async t => {
  const statement = tap.context.db.regular_table.offset(2).limit(1);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'retrieves offset and limited rows');
});
