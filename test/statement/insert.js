import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('inserts and returns a record', async t => {
  const regular_table = tap.context.db.regular_table;

  const value = {
    bool: true,
    num: 123.45,
    txt: 'text'
  };

  const text = await tap.context.db.insert(regular_table, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(regular_table, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 6);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('inserts and returns a record as an object', async t => {
  const regular_table = tap.context.db.regular_table;

  const value = {
    bool: true,
    num: 5.4321,
    txt: 'more'
  };

  const text = await tap.context.db.insert(regular_table, value, tap.context.db.$target.log);
  const row = await tap.context.db.insert(regular_table, value, tap.context.db.$target.one);

  t.matchSnapshot(text, 'matches query text');
  t.ok(row);
  t.equal(row.id, 7);
  t.matchSnapshot(row, 'regular_table matches results');
});

tap.test('inserts and returns multiple records', async t => {
  const regular_table = tap.context.db.regular_table;

  const values = [{
    bool: true,
    num: 123.45,
    txt: 'one'
  }, {
    bool: false,
    num: 678.9,
    txt: 'two'
  }];

  const text = await tap.context.db.insert(regular_table, ...values, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(regular_table, ...values);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('resolves inserting nothing', async t => {
  const regular_table = tap.context.db.regular_table;

  const text = await tap.context.db.insert(regular_table, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(regular_table);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 0);
  t.matchSnapshot(rows, 'insert-nothing matches results');
});

tap.test('sets columns based only on the first value', async t => {
  const regular_table = tap.context.db.regular_table;

  const values = [{
    num: 123.45,
    txt: 'one'
  }, {
    bool: false
  }];

  const text = await tap.context.db.insert(regular_table, ...values, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(regular_table, ...values);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.equal(rows[1].num, null);
  t.equal(rows[1].txt, null);
  t.equal(rows[1].bool, null);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('inserts composite types and domains with pgp.as.format, exprs, or tuples', async t => {
  const has_composite = tap.context.db.has_composite;

  const value = {
    first: {
      rawType: true, // rawType must be set to escape properly!
      // placeholders are for pg-promise and aren't sent to Postgres; args to
      // as.format must be passed in an array since it takes options as a final
      // parameter, meaning as.format is not consistent with the inline-args
      // approach monstrous generally takes
      toPostgres: () => tap.context.db.pgp.as.format('($1, $2)', [-1, 'negative'])
    },
    second: tap.context.db.expr('($1, $2)', 2, 'positive'),
    third: tap.context.db.tuple(3, 'also positive')
  };

  const text = await tap.context.db.insert(has_composite, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(has_composite, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'has_composite matches results');
});

tap.test('ignores superfluous fields', async t => {
  const regular_table = tap.context.db.regular_table;

  const value = {
    bool: true,
    num: 123.45,
    txt: 'text',
    nonexistent: 'ignore me'
  };

  const text = await tap.context.db.insert(regular_table, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(regular_table, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('uses qualified fields', async t => {
  const regular_table = tap.context.db.regular_table;

  const value = {
    [regular_table.$bool]: true,
    [regular_table.$num]: 123.45,
    [regular_table.$txt]: 'text',
    nonexistent: 'ignore me'
  };

  const text = await tap.context.db.insert(regular_table, value, tap.context.db.$target.log);
  const rows = await tap.context.db.insert(regular_table, value);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('errors if given an array of values', async t => {
  const regular_table = tap.context.db.regular_table;

  t.throws(
    () => tap.context.db.insert(regular_table, [{txt: 'one'}, {txt: 'two'}]),
    new Error('values to insert must be objects; use the ... spread operator to pass an array')
  );
});
