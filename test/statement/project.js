import tap from 'tap';
import Projection from '../../lib/statement/project.js';

tap.test('returns empty if given empty data and schema', t => {
  t.matchSnapshot(new Projection().articulate([], {}));

  t.end();
});

tap.test('returns empty if given empty data and non-empty schema', t => {
  t.matchSnapshot(new Projection('id', ['field']).articulate([]));

  t.end();
});

tap.test('throws given a null root key', t => {
  t.throws(() => Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: {
      $key: 'id',
      id: 'id',
      val: 'val'
    }
  }).articulate([
    {id: 1, val: 'p1', children__id: 11, children__val: 'c1'},
    {id: null, val: null, children__id: null, children__val: null}
  ]), new Error('attempted to articulate a row with a null or undefined root key; ensure that all columns are aliased uniquely and/or a complex projection schema defines $key for each output object or array'));

  t.end();
});

tap.test('collapses simple tree structures', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: [{
      $key: 'id',
      id: 'id',
      val: 'val'
    }]
  }).articulate([
    {id: 1, val: 'p1', children__id: 11, children__val: 'c1'},
    {id: 1, val: 'p1', children__id: 12, children__val: 'c2'}
  ]);

  t.equal(data.length, 1);
  t.type(data[0].children, 'Array');
  t.equal(data[0].children.length, 2);
  t.matchSnapshot(data);

  t.end();
});

tap.test('articulates objects', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: {
      $key: 'id',
      id: 'id',
      val: 'val'
    }
  }).articulate({id: 1, val: 'p1', children__id: 11, children__val: 'c1'});

  t.matchSnapshot(data);

  t.end();
});

tap.test('last object wins in the case of accidental object articulation', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: {
      $key: 'id',
      id: 'id',
      val: 'val'
    }
  }).articulate([
    {id: 1, val: 'p1', children__id: 11, children__val: 'c1'},
    {id: 1, val: 'p1', children__id: 12, children__val: 'c2'}
  ]);

  t.equal(data.length, 1);
  t.type(data[0].children, 'object');
  t.matchSnapshot(data);

  t.end();
});

tap.test('articulates partial results', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: [{
      $key: 'id',
      id: 'id',
      val: 'val'
    }]
  }).articulate({id: 1, children__id: 11});

  t.hasStrict(data[0], {val: undefined});
  t.hasStrict(data[0].children[0], {val: undefined});
  t.matchSnapshot(data);

  t.end();
});

tap.test('articulates array fields', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    arr: 'arr',
    children: [{
      $key: 'id',
      id: 'id',
      arr: 'arr'
    }]
  }).articulate({
    id: 1,
    arr: ['one', 'two'],
    children__id: 11,
    children__arr: ['three', 'four']
  });

  t.equal(data[0].arr.length, 2);
  t.equal(data[0].children[0].arr.length, 2);
  t.matchSnapshot(data);

  t.end();
});

tap.skip('can use arrays of column names if no mapping is needed', t => {
  const data = Projection.from({
    $key: 'parent_id',
    parent_id: 'parent_id',
    parent_val: 'parent_val',
    children: {
      $key: 'children_id',
      children_id: 'children_id',
      children_val: 'children_val'
    }
  }).articulate([
    {parent_id: 1, parent_val: 'p1', children_id: 11, children_val: 'c1'},
    {parent_id: 1, parent_val: 'p1', children_id: 12, children_val: 'c2'}
  ]);

  t.matchSnapshot(data); //, [{parent_id: 1, parent_val: 'p1', children: [{children_id: 11, children_val: 'c1'}, {children_id: 12, children_val: 'c2'}]}]);

  t.end();
});

tap.test('sorts children according to its own whims', t => {
  const forward = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: [{
      $key: 'id',
      id: 'id',
      val: 'val'
    }]
  }).articulate([
    {id: 1, val: 'p1', children__id: 11, children__val: 'c2'},
    {id: 1, val: 'p1', children__id: 12, children__val: 'c1'}
  ]);

  const backward = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: [{
      $key: 'id',
      id: 'id',
      val: 'val'
    }]
  }).articulate([
    {id: 1, val: 'p1', children__id: 12, children__val: 'c1'},
    {id: 1, val: 'p1', children__id: 11, children__val: 'c2'}
  ]);

  t.matchSnapshot(forward);
  t.matchSnapshot(backward);
  t.strictSame(forward, backward);

  t.end();
});

tap.test('collapses multiple children with the same parent', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children1: [{ $key: 'id', id: 'id', val: 'val' }],
    children2: [{ $key: 'id', id: 'id', val: 'val' }]
  }).articulate([
    {id: 1, val: 'p1', children1__id: 11, children1__val: 'c1', children2__id: 21, children2__val: 'd1'},
    {id: 1, val: 'p1', children1__id: 12, children1__val: 'c2', children2__id: 22, children2__val: 'd2'},
    {id: 1, val: 'p1', children1__id: 12, children1__val: 'c2', children2__id: 23, children2__val: 'd3'}
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('collapses children into other children', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children1: [{
      $key: 'id',
      id: 'id',
      val: 'val',
      children2: [{
        $key: 'id',
        id: 'id',
        val: 'val'
      }]
    }]
  }).articulate([
    {id: 1, val: 'p1', children1__id: 11, children1__val: 'c1', children1__children2__id: 21, children1__children2__val: 'd1'},
    {id: 1, val: 'p1', children1__id: 12, children1__val: 'c2', children1__children2__id: 22, children1__children2__val: 'd2'},
    {id: 1, val: 'p1', children1__id: 12, children1__val: 'c2', children1__children2__id: 23, children1__children2__val: 'd3'}
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('articulates empty child arrays from null children', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: [{
      $key: 'id',
      id: 'id',
      val: 'val'
    }]
  }).articulate([
    {id: 1, val: 'p1', children__id: null, children__val: null},
    {id: 2, val: 'p2', children__id: 11, children__val: 'c1'}
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('articulates empty child arrays from null children at any level', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: [{
      $key: 'id',
      id: 'id',
      val: 'val',
      grandchildren: [{
        $key: 'id',
        id: 'id',
        val: 'val'
      }]
    }]
  }).articulate([
    {id: 1, val: 'p1', children__id: 11, children__val: 'c1', children__grandchildren__id: 111, children__grandchildren__val: 'g1'},
    {id: 1, val: 'p1', children__id: 11, children__val: 'c1', children__grandchildren__id: 112, children__grandchildren__val: 'g2'},
    {id: 1, val: 'p1', children__id: 12, children__val: 'c2', children__grandchildren__id: 121, children__grandchildren__val: 'g3'},
    {id: 2, val: 'p2', children__id: null, children__val: null, children__grandchildren__id: null, children__grandchildren__val: null},
    {id: 3, val: 'p3', children__id: 31, children__val: 'c3', children__grandchildren__id: null, children__grandchildren__val: null},
    {id: 3, val: 'p3', children__id: 32, children__val: 'c4', children__grandchildren__id: 321, children__grandchildren__val: 'g4'}
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('collapses object descendants', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    child: {
      $key: 'id',
      id: 'id',
      val: 'val',
      grandchild: {
        $key: 'id',
        id: 'id',
        val: 'val'
      }
    }
  }).articulate([
    {id: 1, val: 'p1', child__id: 11, child__val: 'c1', child__grandchild__id: 111, child__grandchild__val: 'g1'}
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('omits null object descendants', t => {
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    child: {
      $key: 'id',
      id: 'id',
      val: 'val',
      grandchild: {
        $key: 'id',
        id: 'id',
        val: 'val'
      }
    }
  }).articulate([
    {id: 1, val: 'p1', child__id: 11, child__val: 'c1', child__grandchild__id: null, child__grandchild__val: null}
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('consolidates duplicate children by pk', t => {
  // this dataset is 'bad' in that you're not usually going to see 100% duplicate rows unless you've really screwed up
  // but it's more legible than reproducing the 'multiple children' data and tests the deduplication just the same
  const data = Projection.from({
    $key: 'id',
    id: 'id',
    val: 'val',
    children: [{
      $key: 'id',
      id: 'id',
      val: 'val'
    }]
  }).articulate([
    {id: 1, val: 'p1', children__id: 11, children__val: 'c1'},
    {id: 1, val: 'p1', children__id: 12, children__val: 'c2'},
    {id: 1, val: 'p1', children__id: 12, children__val: 'c2'}
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('applies new parents only in the correct scope', t => {
  const data = Projection.from({
    $key: 'this_id',
    this_id: 'id',
    this_name: 'name',
    this_notes: 'notes',
    this_archived: 'archived',
    account: {
      $key: 'id',
      id: 'id'
    },
    contact: {
      $key: 'email',
      email: 'email',
      phone: 'phone'
    },
    address: {
      $key: ['coords__latitude', 'coords__longitude'],
      number: 'number',
      street: 'street',
      complement: 'complement',
      neighborhood: 'neighborhood',
      city: 'city',
      state: 'state',
      zipCode: 'zipCode',
      coords: {
        $key: ['latitude', 'longitude'],
        latitude: 'latitude',
        longitude: 'longitude'
      }
    },
    labels: [{
      $key: 'id',
      id: 'id',
      name: 'name',
      color: 'color',
      type: 'type'
    }]
  }).articulate([
    {
      'this_id': 1,
      'account__id': 1,
      'this_name': 'Eduardo Luiz',
      'contact__email': 'email',
      'contact__phone': 'phone',
      'this_notes': null,
      'this_archived': false,
      'address__zipCode': 'zip',
      'address__street': 'street',
      'address__number': 'number',
      'address__complement': null,
      'address__neighborhood': null,
      'address__city': 'Sao Paulo',
      'address__state': 'Sao Paulo',
      'address__coords__latitude': '1',
      'address__coords__longitude': '2',
      'labels__id': '297726d0-301d-4de6-b9a4-e439b81f44ba',
      'labels__name': 'Contrato',
      'labels__color': 'yellow',
      'labels__type': 1
    }, {
      'this_id': 1,
      'account__id': 1,
      'this_name': 'Eduardo Luiz',
      'contact__email': 'email',
      'contact__phone': 'phone',
      'this_notes': null,
      'this_archived': false,
      'address__zipCode': 'zip',
      'address__street': 'street',
      'address__number': 'number',
      'address__complement': null,
      'address__neighborhood': null,
      'address__city': 'Sao Paulo',
      'address__state': 'Sao Paulo',
      'address__coords__latitude': '1',
      'address__coords__longitude': '2',
      'labels__id': '1db6e07f-91e2-42fb-b65c-9a364b6bad4c',
      'labels__name': 'Particular',
      'labels__color': 'purple',
      'labels__type': 1
    }
  ]);

  t.matchSnapshot(data);

  t.end();
});

tap.test('throws if a row is encountered with all-null $key fields', t => {
  t.throws(() => Projection.from({
    $key: ['id_one', 'id_two'],
    id_one: 'id_one',
    id_two: 'id_two',
    val: 'val',
    children1: {
      $key: ['id_one', 'id_two'],
      id_one: 'cid_one',
      id_two: 'cid_two',
      val: 'val',
      children2: {
        $key: ['id_one', 'id_two'],
        id_one: 'ccid_one',
        id_two: 'ccid_two',
        val: 'val'
      }
    }
  }).articulate([{
    id_one: null,
    id_two: null,
    val: 'p1',
    children1__id_one: 11,
    children1__id_two: 12,
    children1__val: 'c1',
    children1__children2__id_one: 21,
    children1__children2__id_two: 22,
    children1__children2__val: 'd1'
  }]), new Error('attempted to articulate a row with a null or undefined root key; ensure that all columns are aliased uniquely and/or a complex projection schema defines $key for each output object or array'));

  t.end();
});

tap.test('accepts and uses key arrays', t => {
  const data = Projection.from({
    $key: ['id_one', 'id_two'],
    id_one: 'id_one',
    id_two: 'id_two',
    val: 'val',
    children1: [{
      $key: ['id_one', 'id_two'],
      id_one: 'id_one',
      id_two: 'id_two',
      val: 'val',
      children2: [{
        $key: ['id_one', 'id_two'],
        id_one: 'id_one',
        id_two: 'id_two',
        val: 'val'
      }]
    }]
  }).articulate([{
    id_one: 1,
    id_two: 2,
    val: 'p1',
    children1__id_one: 11,
    children1__id_two: 12,
    children1__val: 'c1',
    children1__children2__id_one: 21,
    children1__children2__id_two: 22,
    children1__children2__val: 'd1'
  }, {
    id_one: 1,
    id_two: 2,
    val: 'p1',
    children1__id_one: 13,
    children1__id_two: 14,
    children1__val: 'c2',
    children1__children2__id_one: 23,
    children1__children2__id_two: 24,
    children1__children2__val: 'd2'
  }, {
    id_one: 1,
    id_two: 2,
    val: 'p1',
    children1__id_one: 13,
    children1__id_two: 14,
    children1__val: 'c2',
    children1__children2__id_one: 25,
    children1__children2__id_two: 26,
    children1__children2__val: 'd3'
  }, {
    id_one: 3,
    id_two: 4,
    val: 'p2',
    children1__id_one: 15,
    children1__id_two: 16,
    children1__val: 'c3',
    children1__children2__id_one: 27,
    children1__children2__id_two: 28,
    children1__children2__val: 'd4'
  }]);

  t.matchSnapshot(data);

  t.end();
});
