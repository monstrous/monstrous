drop schema public cascade;

create schema public;

create or replace function simple_func (arg int)
  returns int
  language sql
begin atomic
  select arg + 1;
end;

create or replace function two_arg_func (arg1 int, arg2 int)
  returns int
  language sql
begin atomic
  select arg1 + arg2;
end;

create or replace function arr_func (arg1 int[], arg2 numeric[])
  returns int
  language sql
begin atomic
  select cardinality(arg1) + cardinality(arg2);
end;

create or replace function json_func (
  arg1 json,
  arg2 json,
  arg3 jsonb,
  arg4 jsonb,
  arg5 json[],
  arg6 jsonb[]
)
  returns int
  language sql
begin atomic
  select json_array_length(arg2) + jsonb_array_length(arg4);
end;

create or replace function uuid_func (
  arg1 uuid,
  arg2 uuid[]
)
  returns int
  language sql
begin atomic
  select cardinality(arg2) + 1;
end;
