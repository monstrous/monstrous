drop schema if exists public cascade;

create schema public;

create table authors (
  id serial primary key,
  name text not null,
  birth date,
  death date,
  is_in_print boolean not null default false
);

create table books (
  id serial primary key,
  author_id int not null references authors (id),
  title text not null,
  publisher text,
  print_date date
);

create table libraries (
  id serial primary key,
  name text not null,
  postcode text,
  founded date
);

insert into libraries (name, postcode, founded) values
  ('carnegie 1', '12345', '1912-11-13'),
  ('the bookplace', '77788', '2009-03-19');

create table holdings (
  library_id int not null references libraries (id),
  book_id int not null references books(id),
  primary key (library_id, book_id)
);

create table employees (
  id serial not null primary key,
  library_id int not null references libraries (id),
  name text not null,
  position text not null,
  hired_on date
);

insert into employees (library_id, name, position, hired_on) values
  (1, 'Adip Singh', 'director', '2005-01-31'),
  (1, 'Eli T Seddo', 'gopher', '2010-08-16');

create table patrons (
  id serial not null primary key,
  library_id int not null references libraries (id),
  name text not null
);

insert into patrons (library_id, name) values
  (1, 'Jesus Modtempor');
